<?php

error_reporting(0);

header("Access-Control-Allow-Origin: *");
header(
   "Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token , Authorization, X-Requested-With"
);
header("Access-Control-Allow-Methods: POST");
header("Allow: POST");

require_once "database.php";
require_once "eventos.php";
require_once "usuario.php";

$DATA = $_REQUEST;
$query = $DATA["QUERY"];

if ($query == "createEvento") {
   if ($_FILES["image"]["name"]) {
      $img = $_FILES["image"]["name"];
   } else {
      echo json_encode(["error" => "No se encontró imagen."]);
      die();
   }
   createEvento();
} elseif ($query == "updateEvento") {
   updateEvento();
} else {
   echo json_encode(["ERROR" => "SOLICITUD INVALIDA"]);
   die();
}

function createEvento() {
   $path = "../images/";
   $name = strtolower($_FILES["image"]["name"]);
   $res = move_uploaded_file($_FILES["image"]["tmp_name"], $path . $name);
   if ($res) {
      global $DATA;
      $database = new Database();
      $db = $database->getConnection();
      $lo = new Eventos($db);
      $lo->paData = $DATA;
      $lo->paData["IMAGEN"] = $name;
      $llOk = $lo->createEvento();
      if (!$llOk) {
         echo json_encode(["error" => $lo->pcError]);
      } else {
         echo json_encode(["data" => $lo->paDatos]);
      }
   } else {
      echo json_encode(["error" => "Error al subir imagen."]);
   }
}

function updateEvento() {
   global $DATA;
   $database = new Database();
   $db = $database->getConnection();
   $lo = new Eventos($db);
   $lo->paData = $DATA;
   $llOk = false;
   if ($_FILES["image"]["name"]) {
      $path = "../images/";
      if (isset($lo->paData["oldimg"])) {
         $llOk = unlink($path . $lo->paData["oldimg"]);
         if (!$llOk) {
            echo json_encode(["error" => "Error al eliminar imagen antigua"]);
            die();
         }
      }
      $name = strtolower($_FILES["image"]["name"]);
      $res = move_uploaded_file($_FILES["image"]["tmp_name"], $path . $name);
      if ($res) {
         $lo->paData["IMAGEN"] = $name;
         $llOk = $lo->updateEventoWithImg();
      } else {
         echo json_encode(["error" => "Error al actualizar imagen."]);
         die();
      }
   } else {
      $llOk = $lo->updateEventoWithoutImg();
   }
   if (!$llOk) {
      echo json_encode(["error" => $lo->pcError]);
   } else {
      echo json_encode(["data" => $lo->paDatos]);
   }
}

?>

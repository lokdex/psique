<?php

class Eventos {
   private $conn;

   public $paData;
   public $paDatos;
   public $pcError;

   public function __construct($db) {
      $this->conn = $db;
   }

   public function getEventos() {
      $lcSql =
         "SELECT idevno, titulo, resumen, descrip, datos, imagen, link, createdat FROM eventos ORDER BY createdat";
      $stmt = $this->conn->prepare($lcSql);
      $stmt->execute();
      if ($stmt->rowCount() > 0) {
         $eventos = [];
         while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            extract($row);
            $tmp[] = [
               "idevno" => $row["idevno"],
               "titulo" => $row["titulo"],
               "resumen" => $row["resumen"],
               "descrip" => $row["descrip"],
               "datos" => json_decode($row["datos"]),
               "imagen" => $row["imagen"],
               "link" => $row["link"],
               "createdat" => $row["createdat"],
            ];
         }
         $eventos = $tmp;
      } else {
         //http_response_code(400);
         $this->pcError = "No se encontró eventos.";
         return false;
      }
      $this->paDatos = $eventos;
      return true;
   }

   public function getEvento() {
      $lcSql =
         "SELECT idevno, titulo, resumen, descrip, datos, imagen, link, createdat FROM eventos WHERE idevno = :id ORDER BY createdat";
      $stmt = $this->conn->prepare($lcSql);
      $stmt->bindParam(":id", $this->paData["IDEVNO"]);
      $stmt->execute();
      if ($stmt->rowCount() > 0) {
         $row = $stmt->fetch(PDO::FETCH_ASSOC);
         extract($row);
         $tmp = [
            "idevno" => $row["idevno"],
            "titulo" => $row["titulo"],
            "resumen" => $row["resumen"],
            "descrip" => $row["descrip"],
            "datos" => json_decode($row["datos"]),
            "imagen" => $row["imagen"],
            "link" => $row["link"],
            "createdat" => $row["createdat"],
         ];

         $eventos = $tmp;
      } else {
         //http_response_code(400);
         $this->pcError = "Error al recuperar evento.";
         return false;
      }
      $this->paDatos = $eventos;
      return true;
   }

   public function createEvento() {
      $this->paData["DATOS"] = [
         "FECHA" => $this->paData["FECHA"],
         "HORA" => $this->paData["HORA"],
         "MODALI" => $this->paData["MODALI"],
         "IDREUN" => $this->paData["IDREUN"],
         "CODIGO" => $this->paData["CODIGO"],
      ];
      $lcSql = "INSERT INTO eventos (titulo, resumen, descrip, datos, imagen, link, createdat)
                VALUES (:titulo, :resumen, :descrip, :datos, :imagen, :link, NOW())";
      $stmt = $this->conn->prepare($lcSql);
      $stmt->bindParam(":titulo", $this->paData["TITULO"]);
      $stmt->bindParam(":resumen", $this->paData["RESUMEN"]);
      $stmt->bindParam(":descrip", $this->paData["DESCRI"]);
      $stmt->bindParam(":datos", json_encode($this->paData["DATOS"]));
      $stmt->bindParam(":imagen", $this->paData["IMAGEN"]);
      $stmt->bindParam(":link", $this->paData["LINK"]);
      $res = $stmt->execute();
      if (!$res) {
         $this->pcError = "Error al crear evento.";
         return false;
      }
      $this->paDatos = "Evento creado.";
      return true;
   }

   public function updateEventoWithImg() {
      $this->paData["DATOS"] = [
         "FECHA" => $this->paData["FECHA"],
         "HORA" => $this->paData["HORA"],
         "MODALI" => $this->paData["MODALI"],
         "IDREUN" => $this->paData["IDREUN"],
         "CODIGO" => $this->paData["CODIGO"],
      ];
      $lcSql = "UPDATE eventos SET titulo = :titulo, resumen = :resumen, descrip = :descrip, datos = :datos, imagen = :imagen, link = :link
                  WHERE idevno = :id";
      $stmt = $this->conn->prepare($lcSql);
      $stmt->bindParam(":titulo", $this->paData["TITULO"]);
      $stmt->bindParam(":resumen", $this->paData["RESUMEN"]);
      $stmt->bindParam(":descrip", $this->paData["DESCRI"]);
      $stmt->bindParam(":datos", json_encode($this->paData["DATOS"]));
      $stmt->bindParam(":imagen", $this->paData["IMAGEN"]);
      $stmt->bindParam(":link", $this->paData["LINK"]);
      $stmt->bindParam(":id", $this->paData["IDEVNO"]);
      $res = $stmt->execute();
      if (!$res) {
         $this->pcError = "Error al actualizar evento.";
         return false;
      }
      $this->paDatos = "Evento actualizado.";
      return true;
   }

   public function updateEventoWithoutImg() {
      // echo var_dump(intval($this->paData["IDEVNO"]));
      $this->paData["DATOS"] = [
         "FECHA" => $this->paData["FECHA"],
         "HORA" => $this->paData["HORA"],
         "MODALI" => $this->paData["MODALI"],
         "IDREUN" => $this->paData["IDREUN"],
         "CODIGO" => $this->paData["CODIGO"],
      ];
      $cast = intval($this->paData["IDEVNO"]);
      $lcSql = "UPDATE eventos SET titulo = :titulo, resumen = :resumen, descrip = :descrip, datos = :datos, link = :link
                  WHERE idevno = :id";
      $stmt = $this->conn->prepare($lcSql);
      $stmt->bindParam(":titulo", $this->paData["TITULO"]);
      $stmt->bindParam(":resumen", $this->paData["RESUMEN"]);
      $stmt->bindParam(":descrip", $this->paData["DESCRI"]);
      $stmt->bindParam(":datos", json_encode($this->paData["DATOS"]));
      $stmt->bindParam(":link", $this->paData["LINK"]);
      $stmt->bindParam(":id", $cast);
      $res = $stmt->execute();
      if (!$res) {
         $this->pcError = "Error al actualizar evento.";
         return false;
      }
      $this->paDatos = "Evento actualizado.";
      return true;
   }

   public function deleteEvento() {
      $lcSql = "DELETE FROM eventos WHERE idevno = :id";
      $stmt = $this->conn->prepare($lcSql);
      $stmt->bindParam(":id", $this->paData["IDEVNO"]);
      $res = $stmt->execute();
      if (!$res) {
         $this->pcError = "Error al eliminar evento.";
         return false;
      }
      $this->paDatos = "Evento eliminado.";
      return true;
   }
}

?>

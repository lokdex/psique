<?php

class Usuario {
   private $conn;

   public $paData;
   public $paDatos;
   public $pcError;

   public function __construct($db) {
      $this->conn = $db;
   }

   public function iniciarSesion() {
      $lcSql =
         "SELECT idusuario, usuario, passwd FROM usuarios WHERE usuario = :user";
      $stmt = $this->conn->prepare($lcSql);
      $stmt->bindParam(":user", $this->paData["USERNAME"]);
      $stmt->execute();
      if ($stmt->rowCount() == 1) {
         $user = [];
         while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            extract($row);
            $tmp = [
               "idusuario" => $row["idusuario"],
               "usuario" => $row["usuario"],
               "passwd" => $row["passwd"],
            ];
         }
         $user = $tmp;
      } else {
         //http_response_code(400);
         $this->pcError = "No se encontró usuario";
         return false;
      }
      try {
         if ($user["passwd"] != hash("sha512", $this->paData["PASSWORD"])) {
            $this->pcError = "Clave incorrecta";
            return false;
         }
      } catch (Exception $e) {
         $this->pcError = "Error al codificar clave";
         return false;
      }
      $res = ["ID" => $user["idusuario"], "USER" => $user["usuario"]];
      $this->paDatos = $res;
      return true;
   }
}

?>

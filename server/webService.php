<?php

error_reporting(0);

header("Access-Control-Allow-Origin: *");
header(
   "Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token , Authorization, X-Requested-With"
);
header("Access-Control-Allow-Methods: POST");
header("Allow: POST");

require_once "classes/database.php";
require_once "classes/eventos.php";
require_once "classes/usuario.php";

$REQUEST = file_get_contents("php://input");
$DATA = json_decode($REQUEST, true);
$query = $DATA["QUERY"];

if ($query == "getEventos") {
   getEventos();
} elseif ($query == "IniciarSesion") {
   iniciarSesion();
} elseif ($query == "getEvento") {
   getEvento();
} elseif ($query == "deleteEvento") {
   deleteEvento();
} else {
   echo json_encode(["ERROR" => "SOLICITUD INVALIDA"]);
}

function getEventos() {
   global $DATA;
   $database = new Database();
   $db = $database->getConnection();
   $lo = new Eventos($db);
   $lo->paData = $DATA;
   $llOk = $lo->getEventos();
   if (!$llOk) {
      echo json_encode(["error" => $lo->pcError]);
   } else {
      echo json_encode(["data" => $lo->paDatos]);
   }
}

function getEvento() {
   global $DATA;
   $database = new Database();
   $db = $database->getConnection();
   $lo = new Eventos($db);
   $lo->paData = $DATA;
   $llOk = $lo->getEvento();
   if (!$llOk) {
      echo json_encode(["error" => $lo->pcError]);
   } else {
      echo json_encode(["data" => $lo->paDatos]);
   }
}

function deleteEvento() {
   global $DATA;
   $database = new Database();
   $db = $database->getConnection();
   $lo = new Eventos($db);
   $lo->paData = $DATA;
   $path = "./images/";
   $llOk = unlink($path . $lo->paData["IMAGE"]);
   if (!$llOk) {
      echo json_encode(["error" => "Error al eliminar imagen"]);
      die();
   }
   $llOk = $lo->deleteEvento();
   if (!$llOk) {
      echo json_encode(["error" => $lo->pcError]);
   } else {
      echo json_encode(["data" => $lo->paDatos]);
   }
}

function iniciarSesion() {
   global $DATA;
   $database = new Database();
   $db = $database->getConnection();
   $lo = new Usuario($db);
   $lo->paData = $DATA;
   $llOk = $lo->iniciarSesion();
   if (!$llOk) {
      echo json_encode(["error" => $lo->pcError]);
   } else {
      echo json_encode(["data" => $lo->paDatos]);
   }
}

?>

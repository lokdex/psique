import React from "react";
import {
   BrowserRouter as Router,
   Switch,
   Route,
   Redirect,
} from "react-router-dom";

// Import para las views
import { Login } from "./views/Login";
import { EventosCrud } from "./views/EventosCrud";

import { Navigation } from "./views/Navigation";
import { NotFound } from "./views/NotFound";

// componentes
import { PublicRoute, PrivateRoute } from "./components/Routes";

const App = () => {
   return (
      <Router>
         <Switch>
            <Route exact path="/not-found">
               <NotFound />
            </Route>
            <PublicRoute exact path="/login" component={Login} />
            <PrivateRoute exact path="/crud-eventos" component={EventosCrud} />
            <Route path="/">
               <Navigation />
            </Route>
            <Route path="*">
               <Redirect to="/not-found" />
            </Route>
         </Switch>
      </Router>
   );
};

export { App };

import React, { Fragment } from "react";
import { Dialog, Transition, Disclosure } from "@headlessui/react";
import { ChevronUpIcon } from "@heroicons/react/solid";

import AlbertoImg from "../assets/images/Alberto-Boarini.jpg";

const ModalPIT = ({ show, setShow }) => {
   return (
      <Transition.Root show={show} as={Fragment}>
         <Dialog
            as="div"
            static
            className="fixed z-10 inset-0 overflow-y-auto"
            open={show}
            onClose={() => setShow(false)}
         >
            <div className="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
               <Transition.Child
                  as={Fragment}
                  enter="ease-out duration-300"
                  enterFrom="opacity-0"
                  enterTo="opacity-100"
                  leave="ease-in duration-200"
                  leaveFrom="opacity-100"
                  leaveTo="opacity-0"
               >
                  <Dialog.Overlay className="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity" />
               </Transition.Child>

               {/* This element is to trick the browser into centering the modal contents. */}
               <span
                  className="hidden sm:inline-block sm:align-middle sm:h-screen"
                  aria-hidden="true"
               >
                  &#8203;
               </span>
               <Transition.Child
                  as={Fragment}
                  enter="ease-out duration-300"
                  enterFrom="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
                  enterTo="opacity-100 translate-y-0 sm:scale-100"
                  leave="ease-in duration-200"
                  leaveFrom="opacity-100 translate-y-0 sm:scale-100"
                  leaveTo="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
               >
                  <div className="inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle w-10/12">
                     <div className="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
                        <div className="sm:flex sm:items-start">
                           <div className="mt-3 text-center sm:mt-0 sm:ml-4 sm:text-left">
                              <Dialog.Title
                                 as="h2"
                                 className="text-xl leading-6 font-medium text-gray-900"
                              >
                                 La propuesta
                              </Dialog.Title>
                              <div className="mt-2">
                                 <p className="text-md text-gray-500">
                                    El Psicodrama Interno Transgeneracional
                                    (PIT) trabaja con el inconsciente familiar
                                    cuando se introduce en el revelador camino
                                    de la historia familiar o genealogía. Hay
                                    maravillosas obras y teorías que hablan de
                                    nuestros antepasados. Pero trabajamos de
                                    manera diferente. No hablamos de ellos.
                                    Hablamos con ellos. Nuestro objetivo es
                                    aclarar y eliminar las lealtades invisibles
                                    disfuncionales que llevan a las personas a
                                    repetir el argumento de la vida de sus
                                    antepasados. La mera eliminación del síntoma
                                    sin humanización no es terapia.
                                    <br />
                                    Sabemos cómo lidiar con la muerte física.
                                    Pero cuando tenemos ante nosotros una muerte
                                    emocional, ¿Qué herramientas o rituales
                                    tenemos para enfrentar la muerte emocional
                                    de las personas que nos rodean? La principal
                                    forma de muerte emocional es el silencio.
                                    Solo el silencio tiene un alto precio.
                                    Primero se convierte en una enfermedad
                                    física. Romper el silencio es el camino para
                                    que yo encuentre tu presencia, tu ser.
                                    <br />
                                    Cuando representamos lo imposible, lo
                                    imposible se vuelve posible. El psicodrama
                                    se puede definir como una forma de
                                    investigar el alma humana a través de la
                                    acción. Es el método de investigación de
                                    intervención en las relaciones
                                    interpersonales, en grupos, entre grupos o
                                    de una persona consigo misma.
                                 </p>
                                 <h6 className="text-l font-semibold text-gray-900 mt-4">
                                    Metodología
                                 </h6>
                                 <p className="mb-4 text-gray-700">
                                    A diferencia de la constelación familiar de
                                    Bert Hellinger que usa representantes,
                                    Boarini utiliza la imaginación activa y el
                                    psicodrama para trabajar el tiempo y el
                                    espacio dentro del campo morfogenético,
                                    según el científico Rupert Sheldrake. La
                                    formación online abordará los contenidos
                                    teóricos.
                                 </p>
                                 <h6 className="text-l font-semibold text-gray-900">
                                    Dirigido
                                 </h6>
                                 <p className="mb-4 text-gray-700">
                                    El curso está diseñado para abordar la
                                    intervención desde el Psicodrama Interno
                                    Transgeneracional para los psicólogos,
                                    terapeutas holísticos, integrativos,
                                    profesionales de la salud mental.
                                 </p>
                              </div>
                              <div className="flex flex-wrap">
                                 <div className="self-center w-full md:w-1/2 px-4 text-center mt-5">
                                    <img
                                       src={AlbertoImg}
                                       alt=""
                                       style={{
                                          maxHeight: "20rem",
                                          width: "auto",
                                          alignSelf: "center",
                                       }}
                                       className="mx-auto"
                                    />
                                 </div>

                                 <div className="self-center w-full md:w-1/2 px-4 text-left">
                                    <div className="px-4 py-5 flex-auto">
                                       <h6 className="text-l font-semibold text-blue-900">
                                          Sobre Alberto Boarini
                                       </h6>
                                       <p className="mt-2 mb-4 text-gray-700">
                                          De nacionalidad Brasileña, es docente
                                          y supervisor de grupos en España,
                                          Portugal y Perú, además de los 10
                                          estados de Brasil. Conferenciante
                                          ACTIVO en Congresos, seminarios en
                                          Brasil y en el Exterior:
                                       </p>
                                       <ul className="list-disc ml-6">
                                          <li className="mt-2 text-gray-700">
                                             CREADOR, Profesor y Supervisor del
                                             Psicodrama Interno
                                             Transgeneracional
                                          </li>
                                          <li className="text-gray-700">
                                             Certificado en DEEP MEMORY PROCESS®
                                             con Roger Woolger (Inglaterra)
                                          </li>
                                          <li className="text-gray-700">
                                             Graduación en Psicodrama por la
                                             Asociación Brasileña de Psicodrama
                                             (ABPS)
                                          </li>
                                          <li className="text-gray-700">
                                             Certificado por la OPUS
                                          </li>
                                          <li className="text-gray-700">
                                             Psicología y Educación en los
                                             módulos: Anima / Animus, Persona /
                                             Sombra, Mitología Griega, Sueños,
                                             Estudios Alquímicos
                                          </li>
                                          <li className="text-gray-700">
                                             COLOUR FOR LIFE con Mark Wentworth
                                             (Inglaterra)
                                          </li>
                                          <li className="text-gray-700">
                                             PERSONAL MASTERY COACHING con Hans
                                             Tendam (Holanda) por Tasso
                                             International
                                          </li>
                                          <li className="text-gray-700">
                                             Miembro de la AERTH (Asociación
                                             Europea de Terapia Regresiva)
                                          </li>
                                          <li className="text-gray-700">
                                             DOCENTE Y SUPERVISOR de grupos en
                                             ESPAÑA, PORTUGAL y PERÚ, además de
                                             los 10 estados de Brasil.
                                          </li>
                                          <li className="text-gray-700">
                                             Conferenciante ACTIVO en Congresos,
                                             seminarios en Brasil y en el
                                             Exterior.
                                          </li>
                                       </ul>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <h6 className="text-l font-semibold text-blue-900 mb-2">
                           Temario de formación online:
                        </h6>
                        <Disclosure>
                           {({ open }) => (
                              <>
                                 <Disclosure.Button className="flex justify-between w-full px-4 py-2 text-sm font-medium text-left text-blue-900 bg-blue-100 rounded-lg hover:bg-blue-200 focus:outline-none focus-visible:ring focus-visible:ring-blue-500 focus-visible:ring-opacity-75">
                                    <span>1. El drama de los ancestros</span>
                                    <ChevronUpIcon
                                       className={`${
                                          !open ? "transform rotate-180" : ""
                                       } w-5 h-5 text-blue-500`}
                                    />
                                 </Disclosure.Button>
                                 <Transition
                                    show={open}
                                    enter="transition duration-100 ease-out"
                                    enterFrom="transform scale-95 opacity-0"
                                    enterTo="transform scale-100 opacity-100"
                                    leave="transition duration-75 ease-out"
                                    leaveFrom="transform scale-100 opacity-100"
                                    leaveTo="transform scale-95 opacity-0"
                                 >
                                    <Disclosure.Panel className="px-4 pt-4 pb-2 text-sm text-gray-500">
                                       <h6 className="text-l font-semibold text-gray-900">
                                          Psicodrama en otros mundos en otras
                                          realidades
                                       </h6>
                                       <p className="mt-2 mb-4 text-gray-700">
                                          Guía las historias del imaginario aquí
                                          ahora; identificarse con el personaje;
                                          moviendo la trama a tiempo; cómo
                                          dramatizar físicamente; bloqueos y
                                          catarsis; cómo evocar sentimientos.
                                          Transición y trauma entre vidas;
                                          experiencias entre las vidas o los
                                          estados de bardo; sabios y guías;
                                          karma; figuras ancestrales; problemas
                                          inacabados aún están bloqueados;
                                          revisión del trauma; reconciliación y
                                          absolución; integración de fragmentos
                                          del alma; meditación y uso de sabios y
                                          guías; animales de poder y chakras.
                                          <br />
                                          <span className="font-bold">
                                             Duración:
                                          </span>{" "}
                                          2 horas
                                       </p>
                                    </Disclosure.Panel>
                                 </Transition>
                              </>
                           )}
                        </Disclosure>
                        <Disclosure as="div" className="mt-2">
                           {({ open }) => (
                              <>
                                 <Disclosure.Button className="flex justify-between w-full px-4 py-2 text-sm font-medium text-left text-blue-900 bg-blue-100 rounded-lg hover:bg-blue-200 focus:outline-none focus-visible:ring focus-visible:ring-blue-500 focus-visible:ring-opacity-75">
                                    <span>2. El cuerpo</span>
                                    <ChevronUpIcon
                                       className={`${
                                          !open ? "transform rotate-180" : ""
                                       } w-5 h-5 text-blue-500`}
                                    />
                                 </Disclosure.Button>
                                 <Transition
                                    show={open}
                                    enter="transition duration-100 ease-out"
                                    enterFrom="transform scale-95 opacity-0"
                                    enterTo="transform scale-100 opacity-100"
                                    leave="transition duration-75 ease-out"
                                    leaveFrom="transform scale-100 opacity-100"
                                    leaveTo="transform scale-95 opacity-0"
                                 >
                                    <Disclosure.Panel className="px-4 pt-4 pb-2 text-sm text-gray-500">
                                       <h6 className="text-l font-semibold text-gray-900">
                                          Como protagonista experienciador
                                       </h6>
                                       <p className="mt-2 mb-4 text-gray-700">
                                          Imagen corporal o postural; dolor;
                                          problemas orgánicos; observar los
                                          signos corporales y los movimientos de
                                          energía y heridas en el cuerpo
                                          etérico. Repetición crónica de
                                          patrones inacabados; energía congelada
                                          (preguntas inacabadas a nivel físico,
                                          emocional y mental); el cuerpo cuenta
                                          su historia (metáforas corporales);
                                          los tres niveles del cuerpo sutil:
                                          etérico, emocional y mental; patrones
                                          de autolimitación mental y culpa.
                                          <br />
                                          <span className="font-bold">
                                             Duración:
                                          </span>{" "}
                                          2 horas
                                       </p>
                                    </Disclosure.Panel>
                                 </Transition>
                              </>
                           )}
                        </Disclosure>
                        <Disclosure as="div" className="mt-2">
                           {({ open }) => (
                              <>
                                 <Disclosure.Button className="flex justify-between w-full px-4 py-2 text-sm font-medium text-left text-blue-900 bg-blue-100 rounded-lg hover:bg-blue-200 focus:outline-none focus-visible:ring focus-visible:ring-blue-500 focus-visible:ring-opacity-75">
                                    <span>3. Trauma Transgeneracional</span>
                                    <ChevronUpIcon
                                       className={`${
                                          !open ? "transform rotate-180" : ""
                                       } w-5 h-5 text-blue-500`}
                                    />
                                 </Disclosure.Button>
                                 <Transition
                                    show={open}
                                    enter="transition duration-100 ease-out"
                                    enterFrom="transform scale-95 opacity-0"
                                    enterTo="transform scale-100 opacity-100"
                                    leave="transition duration-75 ease-out"
                                    leaveFrom="transform scale-100 opacity-100"
                                    leaveTo="transform scale-95 opacity-0"
                                 >
                                    <Disclosure.Panel className="px-4 pt-4 pb-2 text-sm text-gray-500">
                                       <h6 className="text-l font-semibold text-gray-900">
                                          Romper el silencio
                                       </h6>
                                       <p className="mt-2 mb-4 text-gray-700">
                                          Regresión a la infancia; trabajando en
                                          el "ahora" de la historia; niños
                                          internos inacabados trabajan con los
                                          padres, etc.; el uso de diálogos de
                                          resolución de conflictos. Liberación
                                          de trauma y catarsis en la vida actual
                                          y la vida pasada; entrar en contacto
                                          con los sentimientos reprimidos;
                                          estados disociativos; trabajar con
                                          aliento; resistencia (patrones de
                                          negación y cierre crónico); reacciones
                                          de choque al dolor, la tortura y el
                                          horror.
                                          <br />
                                          <span className="font-bold">
                                             Duración:
                                          </span>{" "}
                                          2 horas
                                       </p>
                                    </Disclosure.Panel>
                                 </Transition>
                              </>
                           )}
                        </Disclosure>
                        <Disclosure as="div" className="mt-2">
                           {({ open }) => (
                              <>
                                 <Disclosure.Button className="flex justify-between w-full px-4 py-2 text-sm font-medium text-left text-blue-900 bg-blue-100 rounded-lg hover:bg-blue-200 focus:outline-none focus-visible:ring focus-visible:ring-blue-500 focus-visible:ring-opacity-75">
                                    <span>4. La Sombra</span>
                                    <ChevronUpIcon
                                       className={`${
                                          !open ? "transform rotate-180" : ""
                                       } w-5 h-5 text-blue-500`}
                                    />
                                 </Disclosure.Button>
                                 <Transition
                                    show={open}
                                    enter="transition duration-100 ease-out"
                                    enterFrom="transform scale-95 opacity-0"
                                    enterTo="transform scale-100 opacity-100"
                                    leave="transition duration-75 ease-out"
                                    leaveFrom="transform scale-100 opacity-100"
                                    leaveTo="transform scale-95 opacity-0"
                                 >
                                    <Disclosure.Panel className="px-4 pt-4 pb-2 text-sm text-gray-500">
                                       <h6 className="text-l font-semibold text-gray-900">
                                          Lo que es bueno para ti es lo
                                          contrario
                                       </h6>
                                       <p className="mt-2 mb-4 text-gray-700">
                                          Para transformar la proyección en un
                                          recurso para el autoconocimiento, es
                                          necesario afrontar el proceso doloroso
                                          de mirarte a ti mismo y poner en
                                          funcionamiento el trabajo psicológico.
                                          Cuanto menos sea consciente la
                                          persona, más serán sus proyecciones
                                          como globos gigantes, flotando más
                                          allá de la realidad. Sin embargo, hay
                                          un gancho. Los objetos de nuestra
                                          proyección están relacionados con
                                          ella.
                                          <br />
                                          <span className="font-bold">
                                             Duración:
                                          </span>{" "}
                                          2 horas
                                       </p>
                                    </Disclosure.Panel>
                                 </Transition>
                              </>
                           )}
                        </Disclosure>
                        <h6 className="text-l font-semibold mt-6">
                           Horarios (Hora Perú: GMT -5)
                        </h6>
                        <p className="mt-2 mb-4 text-gray-700">
                           De 16:00 a 20:00 horas (4 horas por día, total de 10
                           horas pedagógicas)
                           <br />
                           Se incluye breves periodos de descanso.
                        </p>
                        <h6 className="text-l font-semibold mt-6">Valor</h6>
                        <p className="mt-2 mb-4 text-gray-700">
                           $80 dólares americanos.
                           <br />
                           Las reservas se harán con deposito en cuenta
                           bancaria, posteriormente después de presentar el
                           boleto de pago se facilitará el enlace de la
                           plataforma online con el cual se podrá acceder a la
                           formación.
                           <br />
                           Incluye material y certificado virtual firmado por
                           Alberto Boarini.
                        </p>
                     </div>
                     <div className="bg-gray-50 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse">
                        <button
                           type="button"
                           className="mt-3 w-full inline-flex justify-center rounded-md border border-gray-300 shadow-sm px-4 py-2 bg-red-600 text-base font-medium text-white hover:bg-gray-50 hover:text-red-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 sm:mt-0 sm:ml-3 sm:w-auto sm:text-sm"
                           onClick={() => setShow(false)}
                        >
                           Cerrar
                        </button>
                     </div>
                  </div>
               </Transition.Child>
            </div>
         </Dialog>
      </Transition.Root>
   );
};

export default ModalPIT;

import React, { useState } from "react";
import { Link } from "react-router-dom";

import LogoBlanco from "../assets/images/logo_completo_blanco.png";

const NavBar = (props) => {
   const [isOpen, setIsOpen] = useState(false);

   return (
      <>
         <nav
            className={
               (props.transparent
                  ? "top-0 absolute z-50 w-full"
                  : "relative shadow-lg bg-white shadow-lg") +
               " flex flex-wrap items-center justify-between px-2 py-3 navbar-expand-lg"
            }
         >
            <div className="container px-4 mx-auto flex flex-wrap items-center justify-between">
               <div className="w-full relative flex justify-between lg:w-auto lg:static lg:block lg:justify-start">
                  <Link
                     className={
                        (props.transparent ? "text-white" : "text-gray-800") +
                        " text-sm font-bold leading-relaxed inline-block mr-4 py-2 whitespace-no-wrap uppercase"
                     }
                     to="/"
                  >
                     <img
                        alt="Logo"
                        src={LogoBlanco}
                        style={{ width: "6em" }}
                     />
                  </Link>
                  <button
                     className="cursor-pointer text-xl leading-none px-3 py-1 border border-solid border-transparent rounded bg-transparent block lg:hidden outline-none focus:outline-none"
                     type="button"
                     onClick={() => setIsOpen(!isOpen)}
                  >
                     <i
                        className={
                           (props.transparent
                              ? "text-white"
                              : "text-gray-800") + " fas fa-bars"
                        }
                     ></i>
                  </button>
               </div>
               <div
                  className={
                     "lg:flex flex-grow items-center bg-white lg:bg-transparent lg:shadow-none" +
                     (isOpen ? " block rounded shadow-lg" : " hidden")
                  }
                  id="example-navbar-warning"
               >
                  <ul className="flex flex-col lg:flex-row list-none ml-auto -mt-3">
                     <li className="flex items-center">
                        <Link
                           className={
                              (props.transparent
                                 ? "lg:text-white lg:hover:text-gray-300 text-gray-800"
                                 : "text-gray-800 hover:text-gray-600") +
                              " px-3 py-4 lg:py-2 flex items-center text-xs uppercase font-bold"
                           }
                           to="/"
                        >
                           Inicio
                        </Link>
                     </li>
                     <li className="flex items-center">
                        <Link
                           className={
                              (props.transparent
                                 ? "lg:text-white lg:hover:text-gray-300 text-gray-800"
                                 : "text-gray-800 hover:text-gray-600") +
                              " px-3 py-4 lg:py-2 flex items-center text-xs uppercase font-bold"
                           }
                           to="/servicios"
                        >
                           Servicios
                        </Link>
                     </li>
                     <li className="flex items-center">
                        <Link
                           className={
                              (props.transparent
                                 ? "lg:text-white lg:hover:text-gray-300 text-gray-800"
                                 : "text-gray-800 hover:text-gray-600") +
                              " px-3 py-4 lg:py-2 flex items-center text-xs uppercase font-bold"
                           }
                           to="/eventos"
                        >
                           Eventos
                        </Link>
                     </li>
                     <li className="flex items-center">
                        <Link
                           className={
                              (props.transparent
                                 ? "lg:text-white lg:hover:text-gray-300 text-gray-800"
                                 : "text-gray-800 hover:text-gray-600") +
                              " px-3 py-4 lg:py-2 flex items-center text-xs uppercase font-bold"
                           }
                           to="/nosotros"
                        >
                           Nosotros
                        </Link>
                     </li>
                     <li className="flex items-center">
                        <Link
                           className={
                              (props.transparent
                                 ? "lg:text-white lg:hover:text-gray-300 text-gray-800"
                                 : "text-gray-800 hover:text-gray-600") +
                              " px-3 py-4 lg:py-2 flex items-center text-xs uppercase font-bold"
                           }
                           to="/contacto"
                        >
                           Contacto
                        </Link>
                     </li>
                  </ul>
               </div>
            </div>
         </nav>
      </>
   );
};

export { NavBar };

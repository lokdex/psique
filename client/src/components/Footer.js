import React from "react";

const Footer = () => {
   return (
      <>
         <footer className="relative bg-gray-300 pt-8 pb-6">
            <div className="container mx-auto px-4">
               <div className="flex flex-wrap">
                  <div className="w-full lg:w-6/12 px-4">
                     <h4 className="text-3xl font-semibold">Contáctanos!</h4>
                     <h5 className="text-lg mt-0 mb-2 text-gray-700">
                        Encuentranos en redes sociales, o mándanos un mensaje.
                     </h5>
                  </div>
                  <div className="flex flex-col lg:flex-row list-none ml-auto">
                     <div className="flex items-center">
                        <button
                           className="bg-white text-red-400 shadow-lg font-normal h-10 w-10 inline-flex items-center justify-center align-center rounded-full outline-none focus:outline-none mr-2 p-3"
                           type="button"
                        >
                           <i className="flex fab fa-instagram"></i>
                        </button>
                        <button
                           className="bg-white text-blue-600 shadow-lg font-normal h-10 w-10 inline-flex items-center justify-center align-center rounded-full outline-none focus:outline-none mr-2 p-3"
                           type="button"
                        >
                           <i className="flex fab fa-facebook-square"></i>
                        </button>
                        <button
                           className="bg-white text-blue-400 shadow-lg font-normal h-10 w-10 inline-flex items-center justify-center align-center rounded-full outline-none focus:outline-none mr-2 p-3"
                           type="button"
                        >
                           <i className="flex fab fa-twitter"></i>
                        </button>
                        <button
                           className="bg-white text-red-600 shadow-lg font-normal h-10 w-10 inline-flex items-center justify-center align-center rounded-full outline-none focus:outline-none mr-2 p-3"
                           type="button"
                        >
                           <i className="flex fab fa-youtube"></i>
                        </button>
                     </div>
                  </div>
               </div>
               <hr className="my-6 border-gray-400" />
               <div className="flex flex-wrap items-center md:justify-between justify-center">
                  <div className="w-full md:w-4/12 px-4 mx-auto text-center">
                     <div className="text-sm text-gray-600 font-semibold py-1">
                        Copyright © {new Date().getFullYear()} Psique y
                        Movimiento
                     </div>
                  </div>
               </div>
            </div>
         </footer>
      </>
   );
};

export { Footer };

import React, { Fragment } from "react";
import { Dialog, Transition, Disclosure } from "@headlessui/react";
import { ChevronUpIcon } from "@heroicons/react/solid";

import DavidImg from "../assets/images/David-Cuba.jpg";

const ModalArbol = ({ show, setShow }) => {
   return (
      <Transition.Root show={show} as={Fragment}>
         <Dialog
            as="div"
            static
            className="fixed z-10 inset-0 overflow-y-auto"
            open={show}
            onClose={() => setShow(false)}
         >
            <div className="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
               <Transition.Child
                  as={Fragment}
                  enter="ease-out duration-300"
                  enterFrom="opacity-0"
                  enterTo="opacity-100"
                  leave="ease-in duration-200"
                  leaveFrom="opacity-100"
                  leaveTo="opacity-0"
               >
                  <Dialog.Overlay className="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity" />
               </Transition.Child>

               {/* This element is to trick the browser into centering the modal contents. */}
               <span
                  className="hidden sm:inline-block sm:align-middle sm:h-screen"
                  aria-hidden="true"
               >
                  &#8203;
               </span>
               <Transition.Child
                  as={Fragment}
                  enter="ease-out duration-300"
                  enterFrom="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
                  enterTo="opacity-100 translate-y-0 sm:scale-100"
                  leave="ease-in duration-200"
                  leaveFrom="opacity-100 translate-y-0 sm:scale-100"
                  leaveTo="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
               >
                  <div className="inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle w-10/12">
                     <div className="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
                        <div className="sm:flex sm:items-start">
                           <div className="mt-3 text-center sm:mt-0 sm:ml-4 sm:text-left">
                              <Dialog.Title
                                 as="h2"
                                 className="text-xl leading-6 font-medium text-gray-900"
                              >
                                 Descubre todo lo que la psicogenealogía puede
                                 hacer por ti
                              </Dialog.Title>
                              <div className="mt-2">
                                 <p className="text-md text-gray-500">
                                    Taller de base para comprender a la familia
                                    como un sistema colectivo, como se heredan
                                    las funciones, patrones de comportamientos y
                                    creencias, en este taller se adquieren
                                    estrategias para restablecer la armonía del
                                    sistema familiar.
                                    <br />
                                    La familia es un sistema y este sistema
                                    tiene memorias y esas memorias influencian a
                                    cada uno de sus miembros, de manera
                                    consciente o inconsciente determinando su
                                    comportamiento y su forma de vivir.
                                    <br />
                                    Aprenderemos analizar tu árbol genealógico
                                    familiar y como heredamos esa información
                                    atreves de nombres, profesiones, secretos,
                                    enfermedades, modos de amar y modos de
                                    odiar, creencias y vivencias que se repiten
                                    una y otra vez como destino inquebrantable y
                                    que dificultan nuestra vida, hasta que
                                    tomamos conciencia y descubrimos las
                                    vivencias que las están activando.
                                 </p>
                              </div>
                              <div className="flex flex-wrap">
                                 <div className="self-center w-full md:w-1/2 px-4 text-center mt-5">
                                    <img
                                       src={DavidImg}
                                       alt=""
                                       style={{
                                          maxHeight: "20rem",
                                          width: "auto",
                                          alignSelf: "center",
                                       }}
                                       className="mx-auto"
                                    />
                                 </div>

                                 <div className="self-center w-full md:w-1/2 px-4 text-left">
                                    <div className="px-4 py-5 flex-auto">
                                       <h6 className="text-l font-semibold text-blue-900">
                                          Sobre David Cuba
                                       </h6>
                                       <p className="mt-2 mb-4 text-gray-700">
                                          David Cuba, es el fundador de
                                          Metacuántica, un movimiento educativo
                                          de crecimiento interior, dedicado a
                                          ayudar a cambiar vidas, que ya cuenta
                                          con muchos estudiantes y consultantes
                                          en todo el mundo. Ha dedicado más de 7
                                          años a reinventar la experiencia
                                          humana para ayudar a las personas en
                                          su proceso de transformación personal,
                                          a fin de alcanzar un nuevo nivel de
                                          realización.
                                       </p>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <h6 className="text-l font-semibold text-blue-900 mb-2">
                           Temario de formación online:
                        </h6>
                        <ul className="list-disc ml-6">
                           <li className="mt-2 text-gray-700">
                              Orígenes del estudio del árbol genealógico
                           </li>
                           <li className="text-gray-700">
                              Como reparamos y repetimos las vivencias de
                              nuestros ancestros
                           </li>
                           <li className="text-gray-700">
                              Las lealtades familiares invisibles e
                              inconscientes
                           </li>
                           <li className="text-gray-700">
                              Identificar las afinidades transgeneracionales
                              (dobles, Gemelos, Yacentes, evanescentes, heredero
                              universal)
                           </li>
                           <li className="text-gray-700">
                              Las leyes del amor familiar
                           </li>
                           <li className="text-gray-700">
                              Como empezar un proceso de sanación liberando los
                              duelos bloqueados en el árbol genealógico familiar
                           </li>
                           <li className="text-gray-700">
                              Los duelos como medio de sanación de las memorias
                              transgeneracionales.
                           </li>
                           <li className="text-gray-700">
                              Aprender a construir, analizar e identificar
                              patrones en el genograma.
                           </li>
                        </ul>
                        <h6 className="text-l font-semibold mt-6">
                           Horarios (Hora Perú: GMT -5)
                        </h6>
                        <p className="mt-2 mb-4 text-gray-700">
                           De 6:00 pm a 8:30 pm (Duración total de 9 horas)
                        </p>
                        <h6 className="text-l font-semibold mt-6">Inversión:</h6>
                        <p className="mt-2 mb-4 text-gray-700">
                           S/ 200 Soles en Perú
                           <br />
                           $70 dólares americanos en otros países
                        </p>
                     </div>
                     <div className="bg-gray-50 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse">
                        <button
                           type="button"
                           className="mt-3 w-full inline-flex justify-center rounded-md border border-gray-300 shadow-sm px-4 py-2 bg-red-600 text-base font-medium text-white hover:bg-gray-50 hover:text-red-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 sm:mt-0 sm:ml-3 sm:w-auto sm:text-sm"
                           onClick={() => setShow(false)}
                        >
                           Cerrar
                        </button>
                     </div>
                  </div>
               </Transition.Child>
            </div>
         </Dialog>
      </Transition.Root>
   );
};

export default ModalArbol;

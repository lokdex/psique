import React, { useRef, useState, Fragment, useEffect } from "react";
import { Dialog, Transition, Disclosure } from "@headlessui/react";
import { ChevronUpIcon } from "@heroicons/react/solid";

import ImgEvento from "../assets/images/trauma.jpeg";
import axios from "axios";

const ModalEditarEvento = ({
   show,
   setShow,
   evento,
   setEvento,
   getEventos,
}) => {
   let [image, setImage] = useState(
      `http://psiqueymovimiento.com/images/${evento?.imagen}`
   );

   const [selected, setSelected] = useState(evento?.datos?.MODALI);

   function handleChange(event) {
      setImage(URL.createObjectURL(event.target.files[0]));
   }

   const gretelSubmit = async (e) => {
      e.preventDefault();
      try {
         let formData = new FormData(e.target);
         formData.append("QUERY", "updateEvento");
         formData.append("IDEVNO", evento?.idevno);
         let { data: response } = await axios.post(
            "http://psiqueymovimiento.com/classes/uploadForm.php",
            formData
         );
         if (response.data) {
            alert(response.data);
            getEventos();
            setShow(false);
            setEvento(null);
         } else if (response.error) {
            alert(response.error);
         } else {
            throw new Error();
         }
      } catch {
         alert("Error en la solicitud al servidor. (editar)");
      }
   };

   return (
      <Transition.Root show={show} as={Fragment}>
         <Dialog
            as="div"
            static
            className="fixed z-10 inset-0 overflow-y-auto"
            open={show}
            onClose={() => {
               setShow(false);
               setEvento(null);
            }}
         >
            <div className="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
               <Transition.Child
                  as={Fragment}
                  enter="ease-out duration-300"
                  enterFrom="opacity-0"
                  enterTo="opacity-100"
                  leave="ease-in duration-200"
                  leaveFrom="opacity-100"
                  leaveTo="opacity-0"
               >
                  <Dialog.Overlay className="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity" />
               </Transition.Child>

               {/* This element is to trick the browser into centering the modal contents. */}
               <span
                  className="hidden sm:inline-block sm:align-middle sm:h-screen"
                  aria-hidden="true"
               >
                  &#8203;
               </span>
               <Transition.Child
                  as={Fragment}
                  enter="ease-out duration-300"
                  enterFrom="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
                  enterTo="opacity-100 translate-y-0 sm:scale-100"
                  leave="ease-in duration-200"
                  leaveFrom="opacity-100 translate-y-0 sm:scale-100"
                  leaveTo="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
               >
                  <div className="inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle w-10/12">
                     <div className="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
                        <div>
                           <div className="md:grid md:grid-cols-1 md:gap-6">
                              <div className="mt-5 md:mt-0 md:col-span-2">
                                 <form onSubmit={gretelSubmit}>
                                    <div className="shadow sm:rounded-md sm:overflow-hidden">
                                       <div className="px-4 py-5 bg-white space-y-6 sm:p-6">
                                          <div>
                                             <label className="block text-sm font-medium text-gray-700">
                                                Titulo
                                             </label>
                                             <input
                                                type="text"
                                                name="TITULO"
                                                className="shadow-sm mt-1 block w-full sm:text-sm border border-gray-300 rounded-md p-2"
                                                placeholder="Título del evento"
                                                defaultValue={
                                                   evento?.titulo || ""
                                                }
                                             />
                                          </div>

                                          <div>
                                             <label className="block text-sm font-medium text-gray-700">
                                                Resumen
                                             </label>
                                             <textarea
                                                rows={3}
                                                name="RESUMEN"
                                                className="shadow-sm block w-full sm:text-sm border border-gray-300 rounded-md p-2"
                                                placeholder="Resumen del evento"
                                                defaultValue={
                                                   evento?.resumen || ""
                                                }
                                             />
                                          </div>

                                          <div>
                                             <label className="block text-sm font-medium text-gray-700">
                                                Descripción
                                             </label>
                                             <textarea
                                                rows={6}
                                                name="DESCRI"
                                                className="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 mt-1 block w-full sm:text-sm border border-gray-300 rounded-md p-2"
                                                placeholder="Descripción del evento"
                                                defaultValue={
                                                   evento?.descrip || ""
                                                }
                                             />
                                          </div>

                                          <div>
                                             <label className="block text-sm font-medium text-gray-700">
                                                Fecha
                                             </label>
                                             <input
                                                type="text"
                                                name="FECHA"
                                                className="shadow-sm mt-1 block w-full sm:text-sm border border-gray-300 rounded-md p-2"
                                                placeholder="Fecha del evento"
                                                defaultValue={
                                                   evento?.datos?.FECHA || ""
                                                }
                                             />
                                          </div>

                                          <div>
                                             <label className="block text-sm font-medium text-gray-700">
                                                Hora
                                             </label>
                                             <input
                                                type="text"
                                                name="HORA"
                                                className="shadow-sm mt-1 block w-full sm:text-sm border border-gray-300 rounded-md p-2"
                                                placeholder="Hora del evento"
                                                defaultValue={
                                                   evento?.datos?.HORA || ""
                                                }
                                             />
                                          </div>

                                          <div>
                                             <label className="block text-sm font-medium text-gray-700">
                                                Modalidad
                                             </label>
                                             <select
                                                type="text"
                                                name="MODALI"
                                                className="shadow-sm mt-1 block w-full sm:text-sm border border-gray-300 rounded-md p-2"
                                                placeholder="Modalidad del evento"
                                                onChange={(e) =>
                                                   setSelected(e.target.value)
                                                }
                                                value={selected}
                                             >
                                                <option value="P">
                                                   Presencial
                                                </option>
                                                <option value="V">
                                                   Virtual
                                                </option>
                                             </select>
                                          </div>
                                          {selected === "V" ? (
                                             <>
                                                <div>
                                                   <label className="block text-sm font-medium text-gray-700">
                                                      ID de reunión
                                                   </label>
                                                   <input
                                                      type="text"
                                                      name="IDREUN"
                                                      className="shadow-sm mt-1 block w-full sm:text-sm border border-gray-300 rounded-md p-2"
                                                      placeholder="ID de reunión"
                                                      defaultValue={
                                                         evento?.datos?.IDREUN
                                                      }
                                                   />
                                                </div>

                                                <div>
                                                   <label className="block text-sm font-medium text-gray-700">
                                                      Código de acceso
                                                   </label>
                                                   <input
                                                      type="text"
                                                      name="CODIGO"
                                                      className="shadow-sm mt-1 block w-full sm:text-sm border border-gray-300 rounded-md p-2"
                                                      placeholder="Código de acceso"
                                                      defaultValue={
                                                         evento?.datos?.CODIGO
                                                      }
                                                   />
                                                </div>

                                                <div>
                                                   <label className="block text-sm font-medium text-gray-700">
                                                      Link de la reunión
                                                   </label>
                                                   <input
                                                      type="text"
                                                      name="LINK"
                                                      className="shadow-sm mt-1 block w-full sm:text-sm border border-gray-300 rounded-md p-2"
                                                      placeholder="Link de la reunión"
                                                      defaultValue={
                                                         evento?.link
                                                      }
                                                   />
                                                </div>
                                             </>
                                          ) : (
                                             <></>
                                          )}

                                          <div>
                                             <label className="block text-sm font-medium text-gray-700">
                                                Imagen del evento
                                             </label>
                                             <input
                                                type="file"
                                                name="image"
                                                onChange={handleChange}
                                                className="shadow-sm mt-1 block w-full sm:text-sm border border-gray-300 rounded-md p-2"
                                             />
                                             <input
                                                type="hidden"
                                                name="oldimg"
                                                value={evento?.imagen}
                                             />
                                             {!!image && (
                                                <div
                                                   style={{
                                                      display: "flex",
                                                      flexDirection: "column",
                                                   }}
                                                >
                                                   <img
                                                      src={image}
                                                      alt="imagen del evento"
                                                      className="mt-3"
                                                      style={{
                                                         maxHeight: "200px",
                                                         objectFit: "contain",
                                                      }}
                                                   />
                                                </div>
                                             )}
                                          </div>
                                       </div>
                                       <div className="px-4 py-3 bg-gray-50 text-right sm:px-6">
                                          <button
                                             type="button"
                                             className="mx-2 mt-3 w-full inline-flex justify-center rounded-md border border-gray-300 shadow-sm px-4 py-2 bg-red-600 text-base font-medium text-white hover:bg-gray-50 hover:text-red-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 sm:mt-0 sm:ml-3 sm:w-auto sm:text-sm"
                                             onClick={() => {
                                                setShow(false);
                                                setEvento(null);
                                             }}
                                          >
                                             Cerrar
                                          </button>
                                          <button
                                             type="submit"
                                             className="ml-2 inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                                          >
                                             Guardar
                                          </button>
                                       </div>
                                    </div>
                                 </form>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </Transition.Child>
            </div>
         </Dialog>
      </Transition.Root>
   );
};

export default ModalEditarEvento;

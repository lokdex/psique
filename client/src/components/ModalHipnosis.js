import React, { Fragment } from "react";
import { Dialog, Transition, Disclosure } from "@headlessui/react";
import { ChevronUpIcon } from "@heroicons/react/solid";

import BrunoImg from "../assets/images/Bruno-Jean-Bresson.png";

const ModalHipnosis = ({ show, setShow }) => {
   return (
      <Transition.Root show={show} as={Fragment}>
         <Dialog
            as="div"
            static
            className="fixed z-10 inset-0 overflow-y-auto"
            open={show}
            onClose={() => setShow(false)}
         >
            <div className="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
               <Transition.Child
                  as={Fragment}
                  enter="ease-out duration-300"
                  enterFrom="opacity-0"
                  enterTo="opacity-100"
                  leave="ease-in duration-200"
                  leaveFrom="opacity-100"
                  leaveTo="opacity-0"
               >
                  <Dialog.Overlay className="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity" />
               </Transition.Child>

               {/* This element is to trick the browser into centering the modal contents. */}
               <span
                  className="hidden sm:inline-block sm:align-middle sm:h-screen"
                  aria-hidden="true"
               >
                  &#8203;
               </span>
               <Transition.Child
                  as={Fragment}
                  enter="ease-out duration-300"
                  enterFrom="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
                  enterTo="opacity-100 translate-y-0 sm:scale-100"
                  leave="ease-in duration-200"
                  leaveFrom="opacity-100 translate-y-0 sm:scale-100"
                  leaveTo="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
               >
                  <div className="inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle w-10/12">
                     <div className="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
                        <div className="sm:flex sm:items-start">
                           <div className="mt-3 text-center sm:mt-0 sm:ml-4 sm:text-left">
                              <Dialog.Title
                                 as="h2"
                                 className="text-xl leading-6 font-medium text-gray-900"
                              >
                                 La propuesta
                              </Dialog.Title>
                              <div className="mt-2">
                                 <p className="text-md text-gray-500">
                                    Una herramienta muy eficaz en el abordaje de
                                    los problemas humanos es la hipnosis PER
                                    (Psico Espiritual Regresiva). Tomar esta
                                    experiencia formativa bajo la dirección del
                                    creador de la técnica, el maestro Bruno
                                    Bresson, es una experiencia invaluable. Este
                                    curso formativo está previsto para que el
                                    aprendiz pueda empezar a practicar
                                    inducciones hipnóticas a cualquier persona.
                                    La metodología de enseñanza aporta el
                                    beneficio a todos los participantes, sean
                                    terapeutas holísticos, médicos, psicólogos y
                                    en general, para mejorar la vida.
                                    <br />
                                    La certificación avalada por el Instituto de
                                    Investigación y Formación en Reeducación
                                    Emocional I.I.F.R.E., será un certificado
                                    con formación teórica y práctica demostrada
                                    por el maestro Bruno Bresson, del método de
                                    Hipnosis P.E.R. “Hipnosis Psico-Espiritual
                                    Regresiva”.
                                 </p>
                              </div>
                              <div className="flex flex-wrap">
                                 <div className="self-center w-full md:w-1/2 px-4 text-center mt-5">
                                    <img
                                       src={BrunoImg}
                                       alt=""
                                       style={{
                                          maxHeight: "20rem",
                                          width: "auto",
                                          alignSelf: "center",
                                       }}
                                       className="mx-auto"
                                    />
                                 </div>

                                 <div className="self-center w-full md:w-1/2 px-4 text-left">
                                    <div className="px-4 py-5 flex-auto">
                                       <h6 className="text-l font-semibold text-blue-900">
                                          Maestro Hipnoterapeuta Bruno Jean
                                          Bresson
                                       </h6>
                                       <p className="mt-2 mb-4 text-gray-700">
                                          De nacionalidad francesa y actualmente
                                          radicando en España, el maestro viene
                                          trabajando de forma especializada y
                                          durante 29 años ininterrumpidos:
                                       </p>
                                       <ul className="list-disc ml-6">
                                          <li className="mt-2 text-gray-700">
                                             Regresiones temporales y vidas
                                             pasadas
                                          </li>
                                          <li className="text-gray-700">
                                             Hipnosis clínica y espiritual
                                          </li>
                                          <li className="text-gray-700">
                                             Biodescodificación, neuroemoción y
                                             P.N.L.
                                          </li>
                                          <li className="text-gray-700">
                                             Hipnosis P.E.R.
                                             Psico-Espiritual-Regresiva
                                          </li>
                                          <li className="text-gray-700">
                                             Meditación guiada individual y de
                                             grupos
                                          </li>
                                          <li className="text-gray-700">
                                             Mindfullness
                                          </li>
                                       </ul>
                                       <p className="mt-2 mb-4 text-gray-700">
                                          Actualmente, es presidente del
                                          Instituto de Investigación y Formación
                                          en Reeducación Emocional. Ha escrito
                                          el libro “El Secreto de la Hipnosis
                                          Regresiva del Más Allá” y está por
                                          terminar otro denominado “Hipnosis, El
                                          Secreto está en la Emoción”.
                                       </p>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <h6 className="text-l font-semibold text-blue-900 mb-2">
                           Temario Nivel 1:
                        </h6>
                        <Disclosure>
                           {({ open }) => (
                              <>
                                 <Disclosure.Button className="flex justify-between w-full px-4 py-2 text-sm font-medium text-left text-blue-900 bg-blue-100 rounded-lg hover:bg-blue-200 focus:outline-none focus-visible:ring focus-visible:ring-blue-500 focus-visible:ring-opacity-75">
                                    <span>
                                       1. Comportamiento del profesional
                                    </span>
                                    <ChevronUpIcon
                                       className={`${
                                          !open ? "transform rotate-180" : ""
                                       } w-5 h-5 text-blue-500`}
                                    />
                                 </Disclosure.Button>
                                 <Transition
                                    show={open}
                                    enter="transition duration-100 ease-out"
                                    enterFrom="transform scale-95 opacity-0"
                                    enterTo="transform scale-100 opacity-100"
                                    leave="transition duration-75 ease-out"
                                    leaveFrom="transform scale-100 opacity-100"
                                    leaveTo="transform scale-95 opacity-0"
                                 >
                                    <Disclosure.Panel className="px-4 pt-4 pb-2 text-sm text-gray-500">
                                       <ol
                                          style={{
                                             listStyleType: "upper-alpha",
                                          }}
                                          className="ml-6"
                                       >
                                          <li className="text-gray-700">
                                             ¿Con qué?: Complicidad, descubrir
                                             el paciente, intuición… Silencioso
                                             o comunicador o dominante…
                                             Demostraciones.
                                          </li>
                                          <li className="text-gray-700">
                                             Postura: Estar en adelante,
                                             abierto, cercano… Relajado, serio,
                                             amigable… Demostraciones.
                                          </li>
                                          <li className="text-gray-700">
                                             La escucha y la respuesta: Sentir y
                                             hacer sentir, preguntar en el
                                             momento adecuado, escuchar su
                                             inconsciencia… Demostración de
                                             preguntas emocionales o
                                             conscientes.
                                          </li>
                                          <li className="text-gray-700">
                                             Utilidad y utilización del
                                             funcionamiento de la mente
                                             consciente e inconsciente.
                                             Demostraciones de la reacción
                                             consciente e inconsciente y cómo
                                             detectar y sanar los conflictos.
                                          </li>
                                       </ol>
                                    </Disclosure.Panel>
                                 </Transition>
                              </>
                           )}
                        </Disclosure>
                        <Disclosure as="div" className="mt-2">
                           {({ open }) => (
                              <>
                                 <Disclosure.Button className="flex justify-between w-full px-4 py-2 text-sm font-medium text-left text-blue-900 bg-blue-100 rounded-lg hover:bg-blue-200 focus:outline-none focus-visible:ring focus-visible:ring-blue-500 focus-visible:ring-opacity-75">
                                    <span>
                                       2. Técnicas de inducciones. Hacer olvidar
                                       de pensar.
                                    </span>
                                    <ChevronUpIcon
                                       className={`${
                                          !open ? "transform rotate-180" : ""
                                       } w-5 h-5 text-blue-500`}
                                    />
                                 </Disclosure.Button>
                                 <Transition
                                    show={open}
                                    enter="transition duration-100 ease-out"
                                    enterFrom="transform scale-95 opacity-0"
                                    enterTo="transform scale-100 opacity-100"
                                    leave="transition duration-75 ease-out"
                                    leaveFrom="transform scale-100 opacity-100"
                                    leaveTo="transform scale-95 opacity-0"
                                 >
                                    <Disclosure.Panel className="px-4 pt-4 pb-2 text-sm text-gray-500">
                                       <ol
                                          style={{
                                             listStyleType: "upper-alpha",
                                          }}
                                          className="ml-6"
                                       >
                                          <li className="text-gray-700">
                                             Fijación visual: Vela, péndulo,
                                             punto en la mano o techo… Por casos
                                             difíciles o racionales.
                                             Demostración.
                                          </li>
                                          <li className="text-gray-700">
                                             Sensitivas: Respiración, cuerpo,
                                             vista, olfato, tacto, auditivo,
                                             gusto, cinestésica. Los sentidos
                                             P.N.L. Sensitivas: Respiración,
                                             cuerpo, vista, olfato, tacto,
                                             auditivo, gusto, cinestésica. Los
                                             sentidos P.N.L. Demostración.
                                          </li>
                                          <li className="text-gray-700">
                                             La clave importante de las
                                             emociones: Emociones profundas y
                                             escondidas. La Confrontación. La
                                             clave importante de las emociones:
                                             Emociones profundas y escondidas.
                                             Demostración.
                                          </li>
                                          <li className="text-gray-700">
                                             El contacto o la manipulación:
                                             pasar la mano detrás de la persona
                                             de pie y con los ojos cerrados.
                                             Espectáculo. El contacto o la
                                             manipulación: pasar la mano detrás
                                             de la persona de pie y con los ojos
                                             cerrados. Espectáculo.
                                             Demostración.
                                          </li>
                                          <li className="text-gray-700">
                                             Evitar el análisis del paciente:
                                             Evitar que la consciencia este
                                             activándose con el análisis,
                                             perturbar, dominar, evadir,
                                             fascinar o confundir a la persona.
                                          </li>
                                       </ol>
                                    </Disclosure.Panel>
                                 </Transition>
                              </>
                           )}
                        </Disclosure>
                        <Disclosure as="div" className="mt-2">
                           {({ open }) => (
                              <>
                                 <Disclosure.Button className="flex justify-between w-full px-4 py-2 text-sm font-medium text-left text-blue-900 bg-blue-100 rounded-lg hover:bg-blue-200 focus:outline-none focus-visible:ring focus-visible:ring-blue-500 focus-visible:ring-opacity-75">
                                    <span>
                                       3. Etapas de una sesión completa
                                    </span>
                                    <ChevronUpIcon
                                       className={`${
                                          !open ? "transform rotate-180" : ""
                                       } w-5 h-5 text-blue-500`}
                                    />
                                 </Disclosure.Button>
                                 <Transition
                                    show={open}
                                    enter="transition duration-100 ease-out"
                                    enterFrom="transform scale-95 opacity-0"
                                    enterTo="transform scale-100 opacity-100"
                                    leave="transition duration-75 ease-out"
                                    leaveFrom="transform scale-100 opacity-100"
                                    leaveTo="transform scale-95 opacity-0"
                                 >
                                    <Disclosure.Panel className="px-4 pt-4 pb-2 text-sm text-gray-500">
                                       <ol
                                          style={{
                                             listStyleType: "upper-alpha",
                                          }}
                                          className="ml-6"
                                       >
                                          <li className="text-gray-700">
                                             Trabajar la visualización o memoria
                                             para aumentar las emociones: No
                                             decir: “visualiza o mira” pero si:
                                             “Imagina o piensa en o recuerda…”.
                                          </li>
                                          <li className="text-gray-700">
                                             La niñez: Enviar a la niñez
                                             haciendo por ejemplo recordar cómo
                                             siente corriendo el aire en su cara
                                             y asociar a la persona cuando era
                                             niño o sobre un dolor o con algo
                                             relacionado con sus padres o
                                             hermanos o del colegio. No enviar
                                             repentinamente, hacerlo progresivo
                                             para que se incorpore fácilmente.
                                          </li>
                                          <li className="text-gray-700">
                                             Avanzar desde la niñez a cada
                                             momento importante: a. Desde su
                                             niñez a su edad actual.
                                          </li>
                                          <li className="text-gray-700">
                                             El Perdón: A cada momento
                                             necesario, obtener el
                                             entendimiento, la comprensión, la
                                             aceptación y por fin el perdón,
                                             conseguir que se sienta en paz la
                                             persona con la otra y viceversa.
                                             Eliminar cargas del pasado y
                                             obtener la libertad.
                                          </li>
                                          <li className="text-gray-700">
                                             Creación de un jardín interior
                                             secreto.
                                          </li>
                                          <li className="text-gray-700">
                                             La puerta a vidas pasadas: El
                                             monje, en el jardín, el guía
                                             espiritual y avanzar y explorar
                                             dentro de la vida pasada.
                                          </li>
                                          <li className="text-gray-700">
                                             Terminar la vida pasada y
                                             desprenderse del cuerpo hasta
                                             llegar a la luz.
                                          </li>
                                          <li className="text-gray-700">
                                             Conexión con seres difuntos y
                                             conexión con el o los guías y
                                             maestros espirituales.
                                          </li>
                                          <li className="text-gray-700">
                                             Llenarse de luz y energía de
                                             espacio de luz.
                                          </li>
                                          <li className="text-gray-700">
                                             Despedida para el regreso y contar
                                             del uno a cinco: las cinco etapas.
                                          </li>
                                          <li className="text-gray-700">
                                             Comentar un poco la sesión.
                                          </li>
                                          <li className="text-gray-700">
                                             Los intrusos “posesión”.
                                          </li>
                                          <li className="text-gray-700">
                                             Sugestiones post-hipnóticas.
                                          </li>
                                       </ol>
                                    </Disclosure.Panel>
                                 </Transition>
                              </>
                           )}
                        </Disclosure>
                        <Disclosure as="div" className="mt-2">
                           {({ open }) => (
                              <>
                                 <Disclosure.Button className="flex justify-between w-full px-4 py-2 text-sm font-medium text-left text-blue-900 bg-blue-100 rounded-lg hover:bg-blue-200 focus:outline-none focus-visible:ring focus-visible:ring-blue-500 focus-visible:ring-opacity-75">
                                    <span>4. Tabla de sugestiones</span>
                                    <ChevronUpIcon
                                       className={`${
                                          !open ? "transform rotate-180" : ""
                                       } w-5 h-5 text-blue-500`}
                                    />
                                 </Disclosure.Button>
                                 <Transition
                                    show={open}
                                    enter="transition duration-100 ease-out"
                                    enterFrom="transform scale-95 opacity-0"
                                    enterTo="transform scale-100 opacity-100"
                                    leave="transition duration-75 ease-out"
                                    leaveFrom="transform scale-100 opacity-100"
                                    leaveTo="transform scale-95 opacity-0"
                                 >
                                    <Disclosure.Panel className="px-4 pt-4 pb-2 text-sm text-gray-500">
                                       <div className="flex flex-wrap">
                                          <div className="self-center w-full md:w-1/2 px-4 text-left">
                                             <h6 className="text-l font-semibold text-gray-900 mb-2">
                                                Principales:
                                             </h6>
                                             <ul className="list-disc ml-6">
                                                <li className="text-gray-700">
                                                   Directas
                                                </li>
                                                <li className="text-gray-700">
                                                   Indirectas
                                                </li>
                                                <li className="text-gray-700">
                                                   Positivas
                                                </li>
                                                <li className="text-gray-700">
                                                   Negativas
                                                </li>
                                                <li className="text-gray-700">
                                                   Autoritarias
                                                </li>
                                                <li className="text-gray-700">
                                                   Permisivas
                                                </li>
                                                <li className="text-gray-700">
                                                   Con contenidos
                                                </li>
                                                <li className="text-gray-700">
                                                   De procesos
                                                </li>
                                             </ul>
                                          </div>
                                          <div className="self-center w-full md:w-1/2 px-4 text-left">
                                             <h6 className="text-l font-semibold text-gray-900 mb-2">
                                                Precisas y específicas:
                                             </h6>
                                             <ul className="list-disc ml-6">
                                                <li className="text-gray-700">
                                                   De contrario
                                                </li>
                                                <li className="text-gray-700">
                                                   De elecciones idénticas
                                                </li>
                                                <li className="text-gray-700">
                                                   De confusión
                                                </li>
                                                <li className="text-gray-700">
                                                   Aseguradoras
                                                </li>
                                                <li className="text-gray-700">
                                                   Aceptación orientada
                                                </li>
                                                <li className="text-gray-700">
                                                   Metafórica
                                                </li>
                                                <li className="text-gray-700">
                                                   Con presuposiciones
                                                </li>
                                                <li className="text-gray-700">
                                                   Elecciones dirigidas
                                                </li>
                                             </ul>
                                          </div>
                                       </div>
                                    </Disclosure.Panel>
                                 </Transition>
                              </>
                           )}
                        </Disclosure>
                        <Disclosure as="div" className="mt-2">
                           {({ open }) => (
                              <>
                                 <Disclosure.Button className="flex justify-between w-full px-4 py-2 text-sm font-medium text-left text-blue-900 bg-blue-100 rounded-lg hover:bg-blue-200 focus:outline-none focus-visible:ring focus-visible:ring-blue-500 focus-visible:ring-opacity-75">
                                    <span>
                                       5. Tabla de niveles de inducción
                                    </span>
                                    <ChevronUpIcon
                                       className={`${
                                          !open ? "transform rotate-180" : ""
                                       } w-5 h-5 text-blue-500`}
                                    />
                                 </Disclosure.Button>
                                 <Transition
                                    show={open}
                                    enter="transition duration-100 ease-out"
                                    enterFrom="transform scale-95 opacity-0"
                                    enterTo="transform scale-100 opacity-100"
                                    leave="transition duration-75 ease-out"
                                    leaveFrom="transform scale-100 opacity-100"
                                    leaveTo="transform scale-95 opacity-0"
                                 >
                                    <Disclosure.Panel className="px-4 pt-4 pb-2 text-sm text-gray-500">
                                       <ol
                                          style={{
                                             listStyleType: "lower-alpha",
                                          }}
                                          className="ml-6"
                                       >
                                          <li className="text-gray-700">
                                             Estado Beta
                                          </li>
                                          <li className="text-gray-700">
                                             Estado Alfa
                                          </li>
                                          <li className="text-gray-700">
                                             Estado Theta
                                          </li>
                                          <li className="text-gray-700">
                                             Estado Delta
                                          </li>
                                       </ol>
                                    </Disclosure.Panel>
                                 </Transition>
                              </>
                           )}
                        </Disclosure>
                        <Disclosure as="div" className="mt-2">
                           {({ open }) => (
                              <>
                                 <Disclosure.Button className="flex justify-between w-full px-4 py-2 text-sm font-medium text-left text-blue-900 bg-blue-100 rounded-lg hover:bg-blue-200 focus:outline-none focus-visible:ring focus-visible:ring-blue-500 focus-visible:ring-opacity-75">
                                    <span>6. Test de evaluación</span>
                                    <ChevronUpIcon
                                       className={`${
                                          !open ? "transform rotate-180" : ""
                                       } w-5 h-5 text-blue-500`}
                                    />
                                 </Disclosure.Button>
                                 <Transition
                                    show={open}
                                    enter="transition duration-100 ease-out"
                                    enterFrom="transform scale-95 opacity-0"
                                    enterTo="transform scale-100 opacity-100"
                                    leave="transition duration-75 ease-out"
                                    leaveFrom="transform scale-100 opacity-100"
                                    leaveTo="transform scale-95 opacity-0"
                                 >
                                    <Disclosure.Panel className="px-4 pt-4 pb-2 text-sm text-gray-500">
                                       <p className="text-gray-700">
                                          Clasificación de la modalidad
                                          sensorial del cliente.
                                       </p>
                                    </Disclosure.Panel>
                                 </Transition>
                              </>
                           )}
                        </Disclosure>
                        <h6 className="text-l font-semibold text-blue-900 mb-2 mt-6">
                           Temario Nivel 2:
                        </h6>
                        <Disclosure>
                           <Disclosure.Button className="flex justify-between w-full px-4 py-2 text-sm font-medium text-left text-blue-900 bg-blue-100 rounded-lg hover:bg-blue-200 focus:outline-none focus-visible:ring focus-visible:ring-blue-500 focus-visible:ring-opacity-75">
                              <span>
                                 1. Revisión de las inducciones vistas en el
                                 Nivel 1
                              </span>
                           </Disclosure.Button>
                        </Disclosure>
                        <Disclosure as="div" className="mt-2">
                           <Disclosure.Button className="flex justify-between w-full px-4 py-2 text-sm font-medium text-left text-blue-900 bg-blue-100 rounded-lg hover:bg-blue-200 focus:outline-none focus-visible:ring focus-visible:ring-blue-500 focus-visible:ring-opacity-75">
                              <span>
                                 2. Método P.E.R. Etapas, desarrollo general
                              </span>
                           </Disclosure.Button>
                        </Disclosure>
                        <Disclosure as="div" className="mt-2">
                           <Disclosure.Button className="flex justify-between w-full px-4 py-2 text-sm font-medium text-left text-blue-900 bg-blue-100 rounded-lg hover:bg-blue-200 focus:outline-none focus-visible:ring focus-visible:ring-blue-500 focus-visible:ring-opacity-75">
                              <span>
                                 3. C.A.P.P.A.S.: Comprensión. Aceptación.
                                 Perdón. Paz. Armonía. Satisfacción
                              </span>
                           </Disclosure.Button>
                        </Disclosure>
                        <Disclosure as="div" className="mt-2">
                           <Disclosure.Button className="flex justify-between w-full px-4 py-2 text-sm font-medium text-left text-blue-900 bg-blue-100 rounded-lg hover:bg-blue-200 focus:outline-none focus-visible:ring focus-visible:ring-blue-500 focus-visible:ring-opacity-75">
                              <span>
                                 4. R.S.P.: Regresión. Sanación. Progresión
                              </span>
                           </Disclosure.Button>
                        </Disclosure>
                        <Disclosure as="div" className="mt-2">
                           <Disclosure.Button className="flex justify-between w-full px-4 py-2 text-sm font-medium text-left text-blue-900 bg-blue-100 rounded-lg hover:bg-blue-200 focus:outline-none focus-visible:ring focus-visible:ring-blue-500 focus-visible:ring-opacity-75">
                              <span>
                                 5. Biodescodificación emocional o neuro emoción
                                 - Principios y funciones entre cuerpo-mente
                              </span>
                           </Disclosure.Button>
                        </Disclosure>
                        <Disclosure as="div" className="mt-2">
                           <Disclosure.Button className="flex justify-between w-full px-4 py-2 text-sm font-medium text-left text-blue-900 bg-blue-100 rounded-lg hover:bg-blue-200 focus:outline-none focus-visible:ring focus-visible:ring-blue-500 focus-visible:ring-opacity-75">
                              <span>
                                 6. Cómo solucionar los causantes y detonantes
                                 traumáticos
                              </span>
                           </Disclosure.Button>
                        </Disclosure>
                        <Disclosure as="div" className="mt-2">
                           {({ open }) => (
                              <>
                                 <Disclosure.Button className="flex justify-between w-full px-4 py-2 text-sm font-medium text-left text-blue-900 bg-blue-100 rounded-lg hover:bg-blue-200 focus:outline-none focus-visible:ring focus-visible:ring-blue-500 focus-visible:ring-opacity-75">
                                    <span>
                                       7. Adicciones, tabaco, alcohol, drogas,
                                       juegos, sexo, etc
                                    </span>
                                    <ChevronUpIcon
                                       className={`${
                                          !open ? "transform rotate-180" : ""
                                       } w-5 h-5 text-blue-500`}
                                    />
                                 </Disclosure.Button>
                                 <Transition
                                    show={open}
                                    enter="transition duration-100 ease-out"
                                    enterFrom="transform scale-95 opacity-0"
                                    enterTo="transform scale-100 opacity-100"
                                    leave="transition duration-75 ease-out"
                                    leaveFrom="transform scale-100 opacity-100"
                                    leaveTo="transform scale-95 opacity-0"
                                 >
                                    <Disclosure.Panel className="px-4 pt-4 pb-2 text-sm text-gray-500">
                                       <ol
                                          style={{
                                             listStyleType: "upper-alpha",
                                          }}
                                          className="ml-6"
                                       >
                                          <li className="text-gray-700">
                                             Cuál debe ser el comportamiento del
                                             pensamiento ¿Vencedor o luchador?
                                          </li>
                                          <li className="text-gray-700">
                                             Funcionamiento cerebral,
                                             asimilaciones y asociaciones,
                                             manías y gestos, dependencia
                                             física, etc.
                                          </li>
                                          <li className="text-gray-700">
                                             Etapas de las vivencias de nuevas
                                             asociaciones
                                          </li>
                                          <li className="text-gray-700">
                                             Personaje representativo de la
                                             adicción
                                          </li>
                                          <li className="text-gray-700">
                                             Hacer sentir bien y libertad
                                          </li>
                                          <li className="text-gray-700">
                                             Adelgazar con reducción del
                                             estómago virtual
                                          </li>
                                          <li className="text-gray-700">
                                             Demostración
                                          </li>
                                       </ol>
                                    </Disclosure.Panel>
                                 </Transition>
                              </>
                           )}
                        </Disclosure>
                        <Disclosure as="div" className="mt-2">
                           {({ open }) => (
                              <>
                                 <Disclosure.Button className="flex justify-between w-full px-4 py-2 text-sm font-medium text-left text-blue-900 bg-blue-100 rounded-lg hover:bg-blue-200 focus:outline-none focus-visible:ring focus-visible:ring-blue-500 focus-visible:ring-opacity-75">
                                    <span>
                                       8. Crisis de pánico, autovaloración y
                                       complejos
                                    </span>
                                    <ChevronUpIcon
                                       className={`${
                                          !open ? "transform rotate-180" : ""
                                       } w-5 h-5 text-blue-500`}
                                    />
                                 </Disclosure.Button>
                                 <Transition
                                    show={open}
                                    enter="transition duration-100 ease-out"
                                    enterFrom="transform scale-95 opacity-0"
                                    enterTo="transform scale-100 opacity-100"
                                    leave="transition duration-75 ease-out"
                                    leaveFrom="transform scale-100 opacity-100"
                                    leaveTo="transform scale-95 opacity-0"
                                 >
                                    <Disclosure.Panel className="px-4 pt-4 pb-2 text-sm text-gray-500">
                                       <ol
                                          style={{
                                             listStyleType: "upper-alpha",
                                          }}
                                          className="ml-6"
                                       >
                                          <li className="text-gray-700">
                                             Falta de confianza en sí mismo
                                          </li>
                                          <li className="text-gray-700">
                                             Sumisión
                                          </li>
                                          <li className="text-gray-700">
                                             Autoestima
                                          </li>
                                          <li className="text-gray-700">
                                             Complejos
                                          </li>
                                       </ol>
                                    </Disclosure.Panel>
                                 </Transition>
                              </>
                           )}
                        </Disclosure>
                        <Disclosure as="div" className="mt-2">
                           <Disclosure.Button className="flex justify-between w-full px-4 py-2 text-sm font-medium text-left text-blue-900 bg-blue-100 rounded-lg hover:bg-blue-200 focus:outline-none focus-visible:ring focus-visible:ring-blue-500 focus-visible:ring-opacity-75">
                              <span>
                                 9. Sufrimiento amoroso, pérdida de un ser
                                 querido, pérdida del gusto a la vida
                              </span>
                           </Disclosure.Button>
                        </Disclosure>
                        <Disclosure as="div" className="mt-2">
                           <Disclosure.Button className="flex justify-between w-full px-4 py-2 text-sm font-medium text-left text-blue-900 bg-blue-100 rounded-lg hover:bg-blue-200 focus:outline-none focus-visible:ring focus-visible:ring-blue-500 focus-visible:ring-opacity-75">
                              <span>
                                 10. Energías extrañas, intrusos, capacidad de
                                 autoprotección, solución de restos de vidas
                                 pasadas
                              </span>
                           </Disclosure.Button>
                        </Disclosure>
                        <Disclosure as="div" className="mt-2">
                           {({ open }) => (
                              <>
                                 <Disclosure.Button className="flex justify-between w-full px-4 py-2 text-sm font-medium text-left text-blue-900 bg-blue-100 rounded-lg hover:bg-blue-200 focus:outline-none focus-visible:ring focus-visible:ring-blue-500 focus-visible:ring-opacity-75">
                                    <span>
                                       11. Los miedos, las fobias y los bloqueos
                                    </span>
                                    <ChevronUpIcon
                                       className={`${
                                          !open ? "transform rotate-180" : ""
                                       } w-5 h-5 text-blue-500`}
                                    />
                                 </Disclosure.Button>
                                 <Transition
                                    show={open}
                                    enter="transition duration-100 ease-out"
                                    enterFrom="transform scale-95 opacity-0"
                                    enterTo="transform scale-100 opacity-100"
                                    leave="transition duration-75 ease-out"
                                    leaveFrom="transform scale-100 opacity-100"
                                    leaveTo="transform scale-95 opacity-0"
                                 >
                                    <Disclosure.Panel className="px-4 pt-4 pb-2 text-sm text-gray-500">
                                       <ol
                                          style={{
                                             listStyleType: "upper-alpha",
                                          }}
                                          className="ml-6"
                                       >
                                          <li className="text-gray-700">
                                             Altura
                                          </li>
                                          <li className="text-gray-700">
                                             Encerrado
                                          </li>
                                          <li className="text-gray-700">
                                             El mar
                                          </li>
                                          <li className="text-gray-700">
                                             Volar
                                          </li>
                                          <li className="text-gray-700">
                                             La inmensidad
                                          </li>
                                          <li className="text-gray-700">
                                             Impotencia sexual
                                          </li>
                                          <li className="text-gray-700">
                                             No poder hablar en público
                                          </li>
                                       </ol>
                                    </Disclosure.Panel>
                                 </Transition>
                              </>
                           )}
                        </Disclosure>
                        <Disclosure as="div" className="mt-2">
                           <Disclosure.Button className="flex justify-between w-full px-4 py-2 text-sm font-medium text-left text-blue-900 bg-blue-100 rounded-lg hover:bg-blue-200 focus:outline-none focus-visible:ring focus-visible:ring-blue-500 focus-visible:ring-opacity-75">
                              <span>
                                 12. Parálisis del sueño y apnea del sueño
                              </span>
                           </Disclosure.Button>
                        </Disclosure>
                        <Disclosure as="div" className="mt-2">
                           <Disclosure.Button className="flex justify-between w-full px-4 py-2 text-sm font-medium text-left text-blue-900 bg-blue-100 rounded-lg hover:bg-blue-200 focus:outline-none focus-visible:ring focus-visible:ring-blue-500 focus-visible:ring-opacity-75">
                              <span>
                                 13. Vidas pasadas y sus repercusiones en la
                                 vida actual
                              </span>
                           </Disclosure.Button>
                        </Disclosure>
                        <h6 className="text-l font-semibold mt-6">
                           Horarios (Hora Perú: GMT -5)
                        </h6>
                        <p className="mt-2 mb-4 text-gray-700">
                           8:30 am a 1:30 pm y de 3:00 pm a 7:00 pm. <br />
                           Se incluye breves periodos de descanso. <br />
                           Estos horarios están sujetos a eventuales cambios por
                           comodidad de los alumnos.
                        </p>
                        <h6 className="text-l font-semibold mt-6">Inversión</h6>
                        <p className="mt-2 mb-4 text-gray-700">
                           <span className="font-bold">Nivel 1: </span>
                           $500 dólares americanos.
                           <br />
                           Reducción por aceptación de beca parcial de 150
                           dólares por curso con condición de pago hasta el 30
                           de abril. Deberá pagar 350 dólares.
                           <br />
                           <span className="font-bold">Nivel 2: </span>
                           $500 dólares americanos.
                           <br />
                           Reducción por aceptación de beca parcial de 150
                           dólares por curso con condición de pago hasta el 30
                           de abril. Deberá pagar 350 dólares.
                           <br />
                           <span className="font-bold">
                              Nivel 1 + Nivel 2:{" "}
                           </span>
                           $1000 dólares americanos.
                           <br />
                           Reducción por realizar los dos cursos 15% sobre el
                           Máster con condición de pago hasta el 30 de abril
                           (425 dólares del Máster). Total 925 dólares.
                           <br />
                           <span className="font-bold">
                              Alumnos antiguos de Hipnosis PER:{" "}
                           </span>
                           $100 dólares americanos.(pagando hasta el 30 de
                           abril). <br />
                           Alumnos 2020 de formación online: 200 dólares
                           americanos (pagando hasta el 30 de abril).
                           <br />
                        </p>
                        <h6 className="text-l font-semibold mt-6">Nota:</h6>
                        <p className="mt-2 mb-4 text-gray-700">
                           Para postular a la beca tiene que dirigir una
                           solicitud a nombre de Bruno Bresson justificando las
                           razones por las cuales solicita la beca parcial al
                           email: (Arequipa){" "}
                           <span className="font-bold">
                              lduenas@psiqueymovimiento.com
                           </span>
                           . Luego se le informará el resultado de su solicitud.
                           <br />
                           Recuerda que los cupos son limitados y conforme se
                           están inscribiendo se les brinda un espacio. La
                           capacidad no será extendida.
                        </p>
                     </div>
                     <div className="bg-gray-50 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse">
                        <button
                           type="button"
                           className="mt-3 w-full inline-flex justify-center rounded-md border border-gray-300 shadow-sm px-4 py-2 bg-red-600 text-base font-medium text-white hover:bg-gray-50 hover:text-red-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 sm:mt-0 sm:ml-3 sm:w-auto sm:text-sm"
                           onClick={() => setShow(false)}
                        >
                           Cerrar
                        </button>
                     </div>
                  </div>
               </Transition.Child>
            </div>
         </Dialog>
      </Transition.Root>
   );
};

export default ModalHipnosis;

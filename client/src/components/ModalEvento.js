import React, { useRef, useState, Fragment } from "react";
import { Dialog, Transition, Disclosure } from "@headlessui/react";

const ModalEvento = ({ show, setShow, evento }) => {
   return (
      <Transition.Root show={show} as={Fragment}>
         <Dialog
            as="div"
            static
            className="fixed z-10 inset-0 overflow-y-auto"
            open={show}
            onClose={() => setShow(false)}
         >
            <div className="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
               <Transition.Child
                  as={Fragment}
                  enter="ease-out duration-300"
                  enterFrom="opacity-0"
                  enterTo="opacity-100"
                  leave="ease-in duration-200"
                  leaveFrom="opacity-100"
                  leaveTo="opacity-0"
               >
                  <Dialog.Overlay className="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity" />
               </Transition.Child>

               {/* This element is to trick the browser into centering the modal contents. */}
               <span
                  className="hidden sm:inline-block sm:align-middle sm:h-screen"
                  aria-hidden="true"
               >
                  &#8203;
               </span>
               <Transition.Child
                  as={Fragment}
                  enter="ease-out duration-300"
                  enterFrom="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
                  enterTo="opacity-100 translate-y-0 sm:scale-100"
                  leave="ease-in duration-200"
                  leaveFrom="opacity-100 translate-y-0 sm:scale-100"
                  leaveTo="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
               >
                  <div className="inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle w-10/12">
                     <div className="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
                        <div className="sm:flex sm:items-start">
                           <div className="mt-3 text-center sm:mt-0 sm:ml-4 sm:text-left">
                              <div className="flex flex-wrap">
                                 <div className="self-center w-full md:w-1/2 px-4 text-center mt-5">
                                    <img
                                       src={`http://psiqueymovimiento.com/images/${evento?.imagen}`}
                                       alt=""
                                       style={{
                                          maxHeight: "30rem",
                                          width: "auto",
                                          alignSelf: "center",
                                       }}
                                       className="mx-auto"
                                    />
                                 </div>
                                 <div className="self-center mt-2 w-full md:w-1/2">
                                    <p
                                       className="text-md text-blue-600"
                                       style={{ whiteSpace: "pre-line" }}
                                    >
                                       {evento?.descrip}
                                    </p>
                                    <p className="mt-2 mb-4 text-gray-700">
                                       <span className="font-bold">Fecha:</span>{" "}
                                       {evento?.datos?.FECHA}
                                       <br />
                                       <span className="font-bold">
                                          Hora:
                                       </span>{" "}
                                       {evento?.datos?.HORA}
                                       <br />
                                       <span className="font-bold">
                                          Modalidad:
                                       </span>{" "}
                                       {evento?.datos?.MODALI === "V"
                                          ? "Virtual"
                                          : "Presencial"}
                                       {evento?.datos?.MODALI === "V" ? (
                                          <>
                                             <br />
                                             <span className="font-bold">
                                                ID de reunión:
                                             </span>{" "}
                                             {evento?.datos?.IDREUN}
                                             <br />
                                             <span className="font-bold">
                                                Código de acceso:
                                             </span>{" "}
                                             {evento?.datos?.CODIGO}
                                             <br />
                                             <button
                                                type="button"
                                                className="inline-flex justify-center px-4 py-2 text-sm font-medium text-blue-900 bg-blue-100 border border-transparent rounded-md hover:bg-blue-200 focus:outline-none focus-visible:ring-2 focus-visible:ring-offset-2 focus-visible:ring-blue-500"
                                                onClick={() => {
                                                   window.location =
                                                      evento?.link;
                                                }}
                                             >
                                                Unirse a la reunión
                                             </button>
                                          </>
                                       ) : (
                                          <></>
                                       )}
                                    </p>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div className="bg-gray-50 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse">
                        <button
                           type="button"
                           className="mt-3 w-full inline-flex justify-center rounded-md border border-gray-300 shadow-sm px-4 py-2 bg-red-600 text-base font-medium text-white hover:bg-gray-50 hover:text-red-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 sm:mt-0 sm:ml-3 sm:w-auto sm:text-sm"
                           onClick={() => setShow(false)}
                        >
                           Cerrar
                        </button>
                     </div>
                  </div>
               </Transition.Child>
            </div>
         </Dialog>
      </Transition.Root>
   );
};

export default ModalEvento;

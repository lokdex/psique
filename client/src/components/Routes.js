import React from "react";
import { Route, Redirect } from "react-router-dom";

function PublicRoute(props) {
   let user = JSON.parse(sessionStorage.getItem("user"));
   return !!user ? <Redirect to="/" /> : <Route {...props} />;
}

function PrivateRoute(props) {
   let user = JSON.parse(sessionStorage.getItem("user"));
   return !!user ? <Route {...props} /> : <Redirect to="/login" />;
}

export { PublicRoute, PrivateRoute };

import React from "react";
import { Route, Switch, Redirect } from "react-router-dom";

import { Welcome } from "./Welcome";
import { Nosotros } from "./Nosotros";
import { Servicios } from "./Servicios";
import { Eventos } from "./Eventos";
import { Contacto } from "./Contacto";

import { NavBar } from "../components/NavBar";
import { Footer } from "../components/Footer";

const Navigation = () => {
   return (
      <>
         <NavBar transparent />
         <Switch>
            <Route exact path="/">
               <Welcome />
            </Route>
            <Route path="/nosotros">
               <Nosotros />
            </Route>
            <Route path="/servicios">
               <Servicios />
            </Route>
            <Route path="/eventos">
               <Eventos />
            </Route>
            <Route path="/contacto">
               <Contacto />
            </Route>
            <Route path="*">
               <Redirect to="/not-found" />
            </Route>
         </Switch>
         <Footer />
      </>
   );
};

export { Navigation };

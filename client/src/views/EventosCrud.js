import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import axios from "axios";

import ModalCrearEvento from "../components/ModalCrearEvento";
import ModalEditarEvento from "../components/ModalEditarEvento";

const EventosCrud = () => {
   let history = useHistory();
   const [openCrear, setOpenCrear] = useState(false);
   const [openEditar, setOpenEditar] = useState(false);
   const [eventos, setEventos] = useState([]);
   const [evento, setEvento] = useState(null);
   const [idevno, setIdevno] = useState(null);
   const [index, setIndex] = useState(null);

   const getEventos = async () => {
      try {
         const { data: response } = await axios.post(
            "http://psiqueymovimiento.com/webService.php",
            {
               QUERY: "getEventos",
               IDEVNO: idevno,
            }
         );
         if (response.data) {
            setEventos(response.data);
         } else if (response.error) {
            alert(response.error);
            setEventos([]);
         } else {
            throw new Error();
         }
      } catch {
         alert("Error en la solicitud al servidor. (eventos)");
      }
   };

   const getEvento = async () => {
      try {
         let { data: response } = await axios.post(
            "http://psiqueymovimiento.com/webService.php",
            { QUERY: "getEvento", IDEVNO: idevno }
         );
         console.log(response);
         if (response.data) {
            setEvento(response.data);
         } else if (response.error) {
            alert(response.error);
         } else {
            throw new Error();
         }
      } catch {
         alert("Error en la solicitud al servidor. (evento)");
      }
   };

   const handleEditar = () => {
      getEvento();
      setOpenEditar(true);
   };

   const handleEliminar = async () => {
      try {
         let { data: response } = await axios.post(
            "http://psiqueymovimiento.com/webService.php",
            {
               QUERY: "deleteEvento",
               IDEVNO: idevno,
               IMAGE: eventos[index]?.imagen,
            }
         );
         if (response.data) {
            alert("Evento eliminado.");
            getEventos();
         } else if (response.error) {
            alert(response.error);
         } else {
            throw new Error();
         }
      } catch {
         alert("Error en la solicitud al servidor. (eliminar)");
      }
   };

   const handleCerrarSesion = () => {
      sessionStorage.removeItem("user");
      history.push("/");
   };

   useEffect(() => {
      getEventos();
   }, []);

   return (
      <>
         <nav>
            <div className="container px-4 mx-auto flex flex-wrap items-center justify-between">
               <div className="w-full relative flex justify-end lg:w-auto lg:static lg:block lg:justify-end">
                  <button
                     type="button"
                     className="inline-flex justify-center px-4 py-2 text-sm font-medium text-red-900 bg-red-100 border border-transparent rounded-md hover:bg-red-200 focus:outline-none focus-visible:ring-2 focus-visible:ring-offset-2 focus-visible:ring-blue-500"
                     onClick={handleCerrarSesion}
                  >
                     Cerrar sesión
                  </button>
               </div>
            </div>
         </nav>
         <div className="container mx-auto pt-12">
            <div className="flex flex-col">
               <div className="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                  <div className="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                     <div className="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
                        <table className="min-w-full divide-y divide-gray-200">
                           <thead className="bg-gray-50">
                              <tr>
                                 <th
                                    scope="col"
                                    className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                                 >
                                    Título
                                 </th>
                                 <th
                                    scope="col"
                                    className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                                 >
                                    <span style={{ fontSize: "1.75em" }}>
                                       <i class="far fa-check-square"></i>
                                    </span>
                                 </th>
                              </tr>
                           </thead>
                           <tbody className="bg-white divide-y divide-gray-200">
                              {!!eventos?.length ? (
                                 eventos?.map((evento, idx) => (
                                    <tr key={`e-${idx}`}>
                                       <td className="px-6 py-4 whitespace-nowrap">
                                          <div className="flex items-center">
                                             <div className="ml-4">
                                                <div className="text-sm font-medium text-gray-900">
                                                   {evento.titulo}
                                                </div>
                                             </div>
                                          </div>
                                       </td>
                                       <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                                          <input
                                             type="radio"
                                             name="idevno"
                                             value={evento.idevno}
                                             onChange={(e) => {
                                                setIdevno(e.target.value);
                                                setIndex(idx);
                                             }}
                                             checked={idevno === evento.idevno}
                                          ></input>
                                       </td>
                                    </tr>
                                 ))
                              ) : (
                                 <tr>
                                    <td
                                       colSpan={4}
                                       className="text-center font-bold p-3 text-gray-500"
                                    >
                                       NO HAY EVENTOS
                                    </td>
                                 </tr>
                              )}
                           </tbody>
                        </table>
                        <div className="grid grid-cols-3 gap-4 m-3">
                           <button
                              type="button"
                              className="inline-flex justify-center px-4 py-2 text-sm font-medium text-blue-900 bg-blue-100 border border-transparent rounded-md hover:bg-blue-200 focus:outline-none focus-visible:ring-2 focus-visible:ring-offset-2 focus-visible:ring-blue-500"
                              onClick={() => setOpenCrear(true)}
                           >
                              Crear Evento
                           </button>
                           <button
                              type="button"
                              className="inline-flex justify-center px-4 py-2 text-sm font-medium text-blue-900 bg-blue-100 border border-transparent rounded-md hover:bg-blue-200 focus:outline-none focus-visible:ring-2 focus-visible:ring-offset-2 focus-visible:ring-blue-500"
                              onClick={handleEditar}
                           >
                              Editar Evento
                           </button>
                           <button
                              type="button"
                              className="inline-flex justify-center px-4 py-2 text-sm font-medium text-red-900 bg-red-100 border border-transparent rounded-md hover:bg-red-200 focus:outline-none focus-visible:ring-2 focus-visible:ring-offset-2 focus-visible:ring-blue-500"
                              onClick={handleEliminar}
                           >
                              Eliminar Evento
                           </button>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <ModalCrearEvento
            show={openCrear}
            setShow={setOpenCrear}
            getEventos={getEventos}
         />
         {!!evento && (
            <ModalEditarEvento
               show={openEditar}
               setShow={setOpenEditar}
               evento={evento}
               setEvento={setEvento}
               getEventos={getEventos}
            />
         )}
      </>
   );
};

export { EventosCrud };

import React from "react";

import ServiciosImg from "../assets/images/servicios.jpg";

const Servicios = () => {
   return (
      <>
         {/* Hero Begin */}
         <div
            className="relative pt-32 pb-32 flex content-center items-center justify-center"
            style={{
               minHeight: "20vh",
            }}
         >
            <div
               className="absolute top-0 w-full h-full bg-center bg-cover"
               style={{
                  backgroundImage: `url(${ServiciosImg})`,
               }}
            >
               <span
                  id="blackOverlay"
                  className="w-full h-full absolute opacity-75 bg-black"
               ></span>
            </div>
            <div className="container relative mx-auto">
               <div className="items-center flex flex-wrap">
                  <div className="w-full lg:w-6/12 px-4 ml-auto mr-auto text-center">
                     <div className="pr-12">
                        <h1 className="text-white font-semibold text-4xl">
                           Servicios que ofrecemos
                        </h1>
                     </div>
                  </div>
               </div>
            </div>
            <div
               className="top-auto bottom-0 left-0 right-0 w-full absolute pointer-events-none overflow-hidden"
               style={{ height: "70px", transform: "translateZ(0)" }}
            >
               <svg
                  className="absolute bottom-0 overflow-hidden"
                  xmlns="http://www.w3.org/2000/svg"
                  preserveAspectRatio="none"
                  version="1.1"
                  viewBox="0 0 2560 100"
                  x="0"
                  y="0"
               >
                  <polygon
                     className="text-gray-300 fill-current"
                     points="2560 0 2560 100 0 100"
                  ></polygon>
               </svg>
            </div>
         </div>
         {/* Hero End */}

         {/* Cards and small presentation */}
         <section className="pb-20 bg-gray-300 -mt-24">
            <div className="container mx-auto px-4 mt-24">
               <div className="flex flex-wrap">
                  <div className="w-full px-4 text-left mt-10">
                     <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-8 shadow-lg rounded-lg">
                        <div className="px-4 py-5 flex-auto">
                           <div className="text-white p-3 text-center inline-flex items-center justify-center w-12 h-12 mb-5 shadow-lg rounded-full bg-red-400">
                              <i className="fas fa-user"></i>
                           </div>
                           <h6 className="text-2xl font-semibold">
                              Sesiones de psicoterapia individual
                           </h6>
                           <h6 className="text-xl text-gray-800">
                              Modalidad presencial u online
                           </h6>
                           <p className="mt-2 mb-4 text-gray-700">
                              Una experiencia profunda para abordar las raíces
                              causantes de todo aquello que genera malestar,
                              dolor emocional y limita la vida actual. Se busca
                              tomar conciencia de ellas e iniciar el proceso de
                              liberación.
                           </p>
                           <ul>
                              <li className="mt-2 text-gray-700">
                                 Tiempo de atención: 80 minutos
                              </li>
                              <li className="text-gray-700">
                                 Valor: 120 soles <br /> Residentes fuera de
                                 Perú: 40 dólares americanos por PayPal
                              </li>
                           </ul>
                        </div>
                     </div>
                  </div>

                  <div className="w-full px-4 text-left">
                     <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-8 shadow-lg rounded-lg">
                        <div className="px-4 py-5 flex-auto">
                           <div className="text-white p-3 text-center inline-flex items-center justify-center w-12 h-12 mb-5 shadow-lg rounded-full bg-blue-400">
                              <i className="fas fa-users"></i>
                           </div>
                           <h6 className="text-2xl font-semibold">
                              Sesiones de psicoterapia familiar
                           </h6>
                           <h6 className="text-xl text-gray-800">
                              Modalidad presencial u online
                           </h6>
                           <p className="mt-2 mb-4 text-gray-700">
                              Formamos parte de un sistema familiar y las
                              relaciones de pareja o con todos los miembros de
                              la familia, se encuentran fuertemente vinculadas.
                              Un síntoma es en realidad, el síntoma del sistema
                              familiar expresado en uno de sus integrantes.
                              Estas sesiones permiten identificar el origen para
                              que la familia realice los cambios oportunos.
                           </p>
                           <ul>
                              <li className="mt-2 text-gray-700">
                                 Tiempo de atención: 90 minutos
                              </li>
                              <li className="text-gray-700">
                                 Valor: 150 soles
                                 <br /> Residentes fuera de Perú: 50 dólares
                                 americanos por PayPal
                              </li>
                           </ul>
                        </div>
                     </div>
                  </div>

                  <div className="w-full px-4 text-left">
                     <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-8 shadow-lg rounded-lg">
                        <div className="px-4 py-5 flex-auto">
                           <div className="text-white p-3 text-center inline-flex items-center justify-center w-12 h-12 mb-5 shadow-lg rounded-full bg-green-400">
                              <i className="fas fa-users-cog"></i>
                           </div>
                           <h6 className="text-2xl font-semibold">Talleres</h6>
                           <h6 className="text-xl text-gray-800">
                              Modalidad presencial u online
                           </h6>
                           <p className="mt-2 mb-4 text-gray-700">
                              Se abordan diversos temas de crecimiento personal
                              a través de experiencias vivenciales grupales
                              considerando la característica social de la
                              naturaleza humana. El trabajo en grupo permite
                              compartir vivencias y participar de proceso de
                              cambio en conjunto. Es crecer con el otro que te
                              acompaña y al mismo tiempo te permite estar
                              presente junto al grupo en su desarrollo personal.
                           </p>
                           <ul>
                              <li className="mt-2 text-gray-700">
                                 Tiempo: 2 horas
                              </li>
                              <li className="text-gray-700">
                                 Valor: 50 soles
                                 <br /> Residentes fuera de Perú: 15 dólares
                                 americanos por PayPal
                              </li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>

               <div className="w-full px-4 text-left">
                  <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-8 shadow-lg rounded-lg">
                     <div className="px-4 py-5 flex-auto">
                        <div className="text-white p-3 text-center inline-flex items-center justify-center w-12 h-12 mb-5 shadow-lg rounded-full bg-purple-400">
                           <i className="fas fa-school"></i>
                        </div>
                        <h6 className="text-2xl font-semibold">
                           Cursos virtuales
                        </h6>
                        <p className="mt-2 mb-4 text-gray-700">
                           Los cursos virtuales abordan diferentes temas de la
                           psicología integrativa para comprender la historia
                           personal, social, familiar, laboral, espiritual.
                        </p>
                        <div className="ml-6">
                           <ul className="list-disc">
                              <li className="mt-2 text-gray-700">
                                 El poder de las creencias
                              </li>
                              <li className="text-gray-700">
                                 El poder de las emociones
                              </li>
                              <li className="text-gray-700">
                                 Tu fuerza interior
                              </li>
                              <li className="text-gray-700">Cambiando vidas</li>
                              <li className="text-gray-700">
                                 Espiritualidad y trascendencia
                              </li>
                              <li className="text-gray-700">
                                 Superar el duelo
                              </li>
                              <li className="text-gray-700">Adiós ansiedad</li>
                              <li className="text-gray-700">Adiós depresión</li>
                           </ul>
                        </div>
                        <ul>
                           <li className="mt-2 text-gray-700">
                              Tiempo: Por la naturaleza de la modalidad, puede
                              llevarse a su propio ritmo. Revise los detalles
                              del curso.
                           </li>
                           <li className="text-gray-700">
                              Valor: 50 soles
                              <br /> Residentes fuera de Perú: 15 dólares
                              americanos por PayPal
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>

               <div className="w-full px-4 text-left">
                  <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-8 shadow-lg rounded-lg">
                     <div className="px-4 py-5 flex-auto">
                        <div className="text-white p-3 text-center inline-flex items-center justify-center w-12 h-12 mb-5 shadow-lg rounded-full bg-yellow-400">
                           <i className="fas fa-chalkboard-teacher"></i>
                        </div>
                        <h6 className="text-2xl font-semibold">
                           Formación en psicología integrativa
                        </h6>
                        <h6 className="text-xl text-gray-800">
                           Modalidad presencial u online
                        </h6>
                        <p className="mt-2 mb-4 text-gray-700">
                           Las formaciones en psicología integrativa
                           (psicológico, emocional, social, espiritual
                           trascendente) están diseñadas para psicoterapeutas,
                           terapeutas holísticos, u otros que estén interesados
                           en su formación continua con una mirada amplia de la
                           vida humana.
                        </p>
                        <ul>
                           <li className="mt-2 text-gray-700">
                              Tiempo: Sujeto a cronograma
                           </li>
                           <li className="text-gray-700">Valor: Variable</li>
                        </ul>
                     </div>
                  </div>
               </div>

               <div className="w-full px-4 text-left">
                  <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-8 shadow-lg rounded-lg">
                     <div className="px-4 py-5 flex-auto">
                        <div className="text-white p-3 text-center inline-flex items-center justify-center w-12 h-12 mb-5 shadow-lg rounded-full bg-indigo-400">
                           <i className="fas fa-user-friends"></i>
                        </div>
                        <h6 className="text-2xl font-semibold">Mentoría</h6>
                        <p className="mt-2 mb-4 text-gray-700">
                           La mentoría es un espacio de acompañamiento
                           individual y/o grupal destinado a los egresados de
                           psicología que necesitan impulsarse hacia el mundo
                           del trabajo y el éxito. Se aborda el componente
                           psicológico (creencias, pensamientos, emociones y
                           comportamientos), las oportunidades de desarrollo
                           profesional y el emprendimiento, las metas y
                           propósito de vida.
                        </p>
                        <ul>
                           <li className="mt-2 text-gray-700">
                              Tiempo: Promedio 10 sesiones
                           </li>
                           <li className="text-gray-700">
                              Valor: 150 soles por sesión de 90 minutos (accede
                              a promoción por pago anticipado)
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </section>
         {/* End cards */}
      </>
   );
};

export { Servicios };

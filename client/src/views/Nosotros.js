import React from "react";

// Import de imagenes
import Profile from "../assets/images/profile.png";

const Nosotros = () => {
   return (
      <>
         <main className="profile-page">
            <section className="relative block" style={{ height: "500px" }}>
               <div
                  className="absolute top-0 w-full h-full bg-center bg-cover"
                  style={{
                     backgroundImage:
                        "url('https://images.unsplash.com/photo-1499336315816-097655dcfbda?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=2710&q=80')",
                  }}
               >
                  <span
                     id="blackOverlay"
                     className="w-full h-full absolute opacity-50 bg-black"
                  ></span>
               </div>
               <div
                  className="top-auto bottom-0 left-0 right-0 w-full absolute pointer-events-none overflow-hidden"
                  style={{ height: "70px", transform: "translateZ(0)" }}
               >
                  <svg
                     className="absolute bottom-0 overflow-hidden"
                     xmlns="http://www.w3.org/2000/svg"
                     preserveAspectRatio="none"
                     version="1.1"
                     viewBox="0 0 2560 100"
                     x="0"
                     y="0"
                  >
                     <polygon
                        className="text-gray-300 fill-current"
                        points="2560 0 2560 100 0 100"
                     ></polygon>
                  </svg>
               </div>
            </section>
            {/* Sección de presentación del perfil */}
            <section className="relative py-4 bg-gray-300">
               <div className="container mx-auto px-4">
                  <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-6 shadow-xl rounded-lg -mt-64">
                     <div className="px-6">
                        <div className="flex flex-wrap justify-center">
                           <div className="w-full lg:w-3/12 px-4 lg:order-2 flex justify-center">
                              <div className="relative">
                                 <img
                                    alt="..."
                                    src={Profile}
                                    className="shadow-xl rounded-full h-auto align-middle border-none absolute -m-16 -ml-20 lg:-ml-16"
                                    style={{ maxWidth: "150px" }}
                                 />
                              </div>
                           </div>
                        </div>
                        <div className="text-center mt-12">
                           <h3 className="text-4xl font-semibold leading-normal mb-2 text-gray-800 pt-10">
                              Ps. Luz Marina Dueñas Colque
                           </h3>
                           <div className="text-sm leading-normal mt-0 mb-2 text-gray-500 font-bold uppercase">
                              Psicóloga - C.Ps.P. 4564 RNE 010-2013
                           </div>
                           <div className="mb-2 text-gray-700 mt-10">
                              <i className="fas fa-briefcase mr-2 text-lg text-gray-500"></i>
                           </div>
                           <div className="mb-2 text-gray-700">
                              <ul>
                                 <li>
                                    Fundadora y directora de Psique y Movimiento
                                 </li>
                                 <li>
                                    Fundadora de la Maratón de la Felicidad
                                 </li>
                                 <li>
                                    Fundadora de la Red de Psicólogos Educativos
                                    del Colegio de Psicólogos de Arequipa
                                 </li>
                                 <li>
                                    Ex secretaria Regional del Instituto
                                    Latinoamericano de Liderazgo Cristóforo
                                 </li>
                              </ul>
                           </div>
                           <div className="mb-2 text-gray-700 mt-10">
                              <i className="fas fa-university mr-2 text-lg text-gray-500"></i>
                           </div>
                           <div className="mb-2 text-gray-700">
                              <ul>
                                 <li>
                                    Estudios de maestría y doctorado en
                                    Psicología
                                 </li>
                                 <li>
                                    Estudios de maestría en educación superior
                                 </li>
                                 <li>
                                    Máster en Hipnosis PER Psico Espiritual
                                    Regresiva por IIFRE de Bruno Bresson
                                 </li>
                                 <li>
                                    Formación en Psicodrama Interno
                                    Transgeneracional con Alberto Boarini
                                 </li>
                                 <li>
                                    Promotora e impulsora en la instalación del
                                    programa Renovación Matrimonial en la ciudad
                                    de Arequipa
                                 </li>
                                 <li>
                                    Practitioner en Terapia Regresiva
                                    Reconstructiva por la Organización Mundial
                                    de Terapias Regresivas Reconstructivas
                                    Aplicadas
                                 </li>
                                 <li>
                                    Comunicadora en Constelaciones Sistémicas
                                    Familiares por el Centro Peruano de
                                    Constelaciones Sistémicas Familiares
                                 </li>
                              </ul>
                           </div>
                           <div className="mb-2 text-gray-700 mt-10">
                              <i className="fas fa-address-card mr-2 text-lg text-gray-500"></i>
                           </div>
                           <div className="mb-2 text-gray-700">
                              <ul>
                                 <li>24 años como psicóloga educativa</li>
                                 <li>20 años en la docencia universitaria</li>
                                 <li>15 años en consultorio psicológico</li>
                                 <li>Autodidacta Incansable</li>
                              </ul>
                           </div>
                           <div className="mb-2 text-gray-700 mt-10">
                              <i className="fas fa-award mr-2 text-lg text-gray-500"></i>
                           </div>
                           <div className="mb-2 text-gray-700">
                              <ul>
                                 <li>
                                    Certificación Profesional por SINEACE –
                                    Primera psicóloga certificada del sur del
                                    país 2020 en evaluación, diagnóstico,
                                    consejería y tratamiento
                                 </li>
                                 <li>
                                    25 años de colegiatura en el Colegio de
                                    Psicólogos del Perú
                                 </li>
                              </ul>
                           </div>
                        </div>
                        <div className="mt-10 py-10 border-t border-gray-300 text-center">
                           <div className="flex flex-wrap justify-center">
                              <div className="w-full lg:w-9/12 px-4">
                                 <p className="mb-4 text-lg leading-relaxed text-gray-800">
                                    Me siento feliz de ofrecerte un espacio
                                    nutritivo para llenar la existencia de
                                    bienestar, despertar conciencia, trascender
                                    y lograr la vida abundante que mereces como
                                    persona. Así mismo, para los profesionales
                                    de la salud mental, terapeutas integrativos,
                                    holísticos; les ofrezco una oportunidad
                                    maravillosa para la formación en
                                    psicoterapia integrativa con una mirada
                                    amplia de la vida.
                                 </p>
                                 <p className="mb-4 text-lg leading-relaxed text-gray-800">
                                    Llevo más de 25 años de ejercicio
                                    profesional ininterrumpido de la psicología.
                                    Mi vida cambió hace más de 10 años, cuando
                                    decidí integrar la espiritualidad en mis
                                    sesiones de psicoterapia, logrando
                                    rápidamente los procesos de cambio en las
                                    personas que atendía en consultorio.
                                 </p>
                                 <h4 className="text-2xl font-semibold leading-normal mb-2 text-gray-800 mb-2">
                                    Me siento agradecida por todo lo que he
                                    recibido y quiero compartir contigo mi
                                    pasión por la psicología y mi fuerte deseo
                                    de ayudarte a crecer para lograr tus
                                    objetivos y llegar a vivir la vida que te
                                    corresponde.
                                 </h4>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
            {/* Visión y Misión */}
            <section className="relative bg-gray-300">
               <div className="container mx-auto px-4">
                  <div className="flex flex-wrap">
                     <div className="w-full md:w-6/12 px-4 text-center">
                        <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-8 shadow-lg rounded-lg">
                           <div className="px-4 py-5 flex-auto">
                              <div className="text-white p-3 text-center inline-flex items-center justify-center w-12 h-12 mb-5 shadow-lg rounded-full bg-red-400">
                                 <i className="fas fa-check-circle"></i>
                              </div>
                              <h6 className="text-xl font-semibold">Visión</h6>
                              <p className="mt-2 mb-4 text-gray-600">
                                 Ser una institución reconocida por su alta
                                 calidad en la psicología, la formación en
                                 psicoterapia, el desarrollo de la calidad de
                                 vida de las personas y la ejecución de eventos
                                 formativos de crecimiento personal.
                              </p>
                           </div>
                        </div>
                     </div>

                     <div className="w-full md:w-6/12 px-4 text-center">
                        <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-8 shadow-lg rounded-lg">
                           <div className="px-4 py-5 flex-auto">
                              <div className="text-white p-3 text-center inline-flex items-center justify-center w-12 h-12 mb-5 shadow-lg rounded-full bg-blue-400">
                                 <i className="far fa-check-circle"></i>
                              </div>
                              <h6 className="text-xl font-semibold">Misión</h6>
                              <p className="mt-2 mb-4 text-gray-600">
                                 Ofrecer las mejores herramientas integrativas
                                 de la psicología para abordar la vida humana
                                 buscando el equilibrio psico-socio-emocional,
                                 biológico y espiritual.
                              </p>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div className="container mx-auto px-4">
                  <div className="flex flex-wrap">
                     <div className="w-full md:w-6/12 px-4 text-center">
                        <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-8 shadow-lg rounded-lg">
                           <div className="px-4 py-5 flex-auto">
                              <div className="text-white p-3 text-center inline-flex items-center justify-center w-12 h-12 mb-5 shadow-lg rounded-full bg-purple-400">
                                 <i className="fas fa-list-ul"></i>
                              </div>
                              <h6 className="text-xl font-semibold">Valores</h6>
                              <div className="mt-2 mb-4 text-gray-600">
                                 <ul>
                                    <li>Respeto a la vida</li>
                                    <li>Responsabilidad</li>
                                    <li>Ética</li>
                                    <li>Empatía</li>
                                    <li>Compromiso social</li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                     </div>

                     <div className="w-full md:w-6/12 px-4 text-center">
                        <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-8 shadow-lg rounded-lg">
                           <div className="px-4 py-5 flex-auto">
                              <div className="text-white p-3 text-center inline-flex items-center justify-center w-12 h-12 mb-5 shadow-lg rounded-full bg-green-400">
                                 <i className="fas fa-bullseye"></i>
                              </div>
                              <h6 className="text-xl font-semibold">
                                 Objetivos de la empresa
                              </h6>
                              <div className="mt-2 mb-4 text-gray-600">
                                 <ol type="a">
                                    <li>
                                       Ser una institución comprometida con la
                                       salud psico-socio-emocional y espiritual
                                       de la sociedad.
                                    </li>
                                    <li>
                                       Ofrecer servicios en la prevención,
                                       promoción, e intervención en la salud
                                       psicológica de las personas.
                                    </li>
                                    <li>
                                       Mantener principios de respeto por la
                                       vida humana.
                                    </li>
                                 </ol>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
         </main>
      </>
   );
};

export { Nosotros };

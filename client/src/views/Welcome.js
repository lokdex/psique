import React from "react";

// Import de imagenes
import HeroImg from "../assets/images/hero_img.jpg";

const Welcome = () => {
   return (
      <>
         {/* Hero Begin */}
         <div
            className="relative pt-16 pb-32 flex content-center items-center justify-center"
            style={{
               minHeight: "75vh",
            }}
         >
            <div
               className="absolute top-0 w-full h-full bg-center bg-cover"
               style={{
                  backgroundImage: `url(${HeroImg})`,
               }}
            >
               <span
                  id="blackOverlay"
                  className="w-full h-full absolute opacity-75 bg-black"
               ></span>
            </div>
            <div className="container relative mx-auto">
               <div className="items-center flex flex-wrap">
                  <div className="w-full lg:w-6/12 px-4 ml-auto mr-auto text-center">
                     <div className="pr-12">
                        <h1 className="text-white font-semibold text-5xl">
                           Psique y Movimiento
                        </h1>
                        <p className="mt-4 text-lg text-gray-300">
                           Cambios internos producen cambios externos
                        </p>
                     </div>
                  </div>
               </div>
            </div>
            <div
               className="top-auto bottom-0 left-0 right-0 w-full absolute pointer-events-none overflow-hidden"
               style={{ height: "70px", transform: "translateZ(0)" }}
            >
               <svg
                  className="absolute bottom-0 overflow-hidden"
                  xmlns="http://www.w3.org/2000/svg"
                  preserveAspectRatio="none"
                  version="1.1"
                  viewBox="0 0 2560 100"
                  x="0"
                  y="0"
               >
                  <polygon
                     className="text-gray-300 fill-current"
                     points="2560 0 2560 100 0 100"
                  ></polygon>
               </svg>
            </div>
         </div>
         {/* Hero End */}

         {/* Cards and small presentation */}
         <section className="pb-20 bg-gray-300 -mt-24">
            <div className="container mx-auto px-4">
               <div className="flex flex-wrap">
                  <div className="lg:pt-12 pt-6 w-full md:w-4/12 px-4 text-center">
                     <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-8 shadow-lg rounded-lg">
                        <div className="px-4 py-5 flex-auto">
                           <div className="text-white p-3 text-center inline-flex items-center justify-center w-12 h-12 mb-5 shadow-lg rounded-full bg-red-400">
                              <i className="fas fa-award"></i>
                           </div>
                           <h6 className="text-xl font-semibold">
                              Sesiones de psicoterapia
                           </h6>
                           <p className="mt-2 mb-4 text-gray-600">
                              Es un espacio diseñado para abordar con sutileza
                              todo aquello que limita el desarrollo pleno de la
                              persona. Tiene como objetivo acompañar en los
                              procesos de cambio y crecimiento personal.
                              <br />
                              Tiene la modalidad presencial y online.
                           </p>
                        </div>
                     </div>
                  </div>

                  <div className="w-full md:w-4/12 px-4 text-center">
                     <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-8 shadow-lg rounded-lg">
                        <div className="px-4 py-5 flex-auto">
                           <div className="text-white p-3 text-center inline-flex items-center justify-center w-12 h-12 mb-5 shadow-lg rounded-full bg-blue-400">
                              <i className="fas fa-retweet"></i>
                           </div>
                           <h6 className="text-xl font-semibold">Talleres</h6>
                           <p className="mt-2 mb-4 text-gray-600">
                              Son experiencias grupales integrales de desarrollo
                              psicológico, emocional, conductual, espiritual.
                              Tiene como objetivo sentirse parte de un grupo
                              social y juntos compartir experiencias para
                              evolucionar hacia la mejor versión de sí mismos.
                              <br />
                              Tiene la modalidad presencial y online.
                           </p>
                        </div>
                     </div>
                  </div>

                  <div className="pt-6 w-full md:w-4/12 px-4 text-center">
                     <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-8 shadow-lg rounded-lg">
                        <div className="px-4 py-5 flex-auto">
                           <div className="text-white p-3 text-center inline-flex items-center justify-center w-12 h-12 mb-5 shadow-lg rounded-full bg-green-400">
                              <i className="fas fa-fingerprint"></i>
                           </div>
                           <h6 className="text-xl font-semibold">Eventos</h6>
                           <p className="mt-2 mb-4 text-gray-600">
                              Se realizan webinar, conferencias, charlas, entre
                              otros, donde se abordan temas de la psicología y
                              la vida humana.
                              <br />
                              Tiene la modalidad presencial y online.
                           </p>
                        </div>
                     </div>
                  </div>
               </div>

               <div className="flex flex-wrap items-center mt-32">
                  <div className="w-full md:w-5/12 px-4 mr-auto ml-auto">
                     <div className="text-gray-600 p-3 text-center inline-flex items-center justify-center w-16 h-16 mb-6 shadow-lg rounded-full bg-gray-100">
                        <i className="fas fa-user-friends text-xl"></i>
                     </div>
                     <h3 className="text-3xl mb-2 font-semibold leading-normal">
                        Estamos para ayudarte
                     </h3>
                     <p className="text-lg font-light leading-relaxed mt-4 mb-4 text-gray-700">
                        Nuestro propósito es acompañarte en tu proceso de
                        desarrollo hacia una vida plena y abundante. Con todos
                        nuestros años de experiencia podemos impulsarte a tomar
                        un nuevo rumbo en tu vida.
                     </p>
                     <p className="text-lg font-light leading-relaxed mt-0 mb-4 text-gray-700">
                        En Psique y Movimiento, nos enfocamos en ayudarte a
                        superar adecuadamente cualquier problema, a romper
                        limitaciones, a vencer tus miedos y ser una persona
                        satisfecha de sí misma y la vida que lleva.
                     </p>
                  </div>

                  <div className="w-full md:w-4/12 px-4 mr-auto ml-auto">
                     <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-6 shadow-lg rounded-lg bg-pink-600">
                        <img
                           alt="..."
                           src="https://images.unsplash.com/photo-1522202176988-66273c2fd55f?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1051&q=80"
                           className="w-full align-middle rounded-t-lg"
                        />
                        <blockquote className="relative p-8 mb-4">
                           <svg
                              preserveAspectRatio="none"
                              xmlns="http://www.w3.org/2000/svg"
                              viewBox="0 0 583 95"
                              className="absolute left-0 w-full block"
                              style={{
                                 height: "95px",
                                 top: "-94px",
                              }}
                           >
                              <polygon
                                 points="-30,95 583,95 583,65"
                                 className="text-pink-600 fill-current"
                              ></polygon>
                           </svg>
                           <h4 className="text-xl font-bold text-white">
                              Atención humana
                           </h4>
                           <p className="text-md font-light mt-2 text-white">
                              Una nueva vida te espera, el cambio empieza en ti
                           </p>
                        </blockquote>
                     </div>
                  </div>

                  <div className="self-center w-full pt-10 px-4 text-center">
                     <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-8 shadow-lg rounded-lg">
                        <div className="px-4 py-5 flex-auto">
                           <div className="text-white p-3 text-center inline-flex items-center justify-center w-12 h-12 mb-5 shadow-lg rounded-full bg-blue-400">
                              <i className="fas fa-comment"></i>
                           </div>
                           <h6 className="text-2xl font-semibold">
                              Testimonios
                           </h6>
                           <p className="mt-2 mb-4 text-gray-700">
                              "Hacer terapia con la Dra. Luz Dueñas me ayudó a
                              ser consciente de que mi subconsciente almacenó
                              desde mi niñez muchos aspectos negativos que
                              afectaban mi vida. Fue bueno descubrir y empezar a
                              trabajar sobre ello para mejorar mi salud. Sé que
                              es un camino largo a seguir, pero con la dirección
                              de la Dra. Luz todo es posible."
                           </p>
                           <p>- Marleni Matuti, EEUU</p>
                           <p className="mt-2 mb-4 text-gray-700">
                              "La experiencia con Luz Dueñas me permitió poder
                              interconectar muchas ideas desde mi pasado hacia
                              mi presente, aportándome nuevas perspectivas y
                              conocimientos de gran ayuda para mi proyecto de
                              vida."
                           </p>
                           <p>
                              - Joan Egea Barber (Formador y escritor, Autor de
                              Vibra+Sistémica), España
                           </p>
                           <p className="mt-2 mb-4 text-gray-700">
                              "Llevar psicoterapia con la Ps. Luz, me hizo
                              sentir bien conmigo misma, entender mejores cosas
                              que yo creía diferente, aprendí a canalizar mis
                              emociones brindándome herramientas para
                              utilizarlas en momentos de crisis. Ir a
                              psicoterapia tiene muchos beneficios, por eso me
                              gustaría que las personas que padecen de
                              enfermedades como la ansiedad, la depresión,
                              tuviesen la misma oportunidad de recibir este tipo
                              de ayuda. <br />
                              Gracias Ps. Luz. Dios la bendiga."
                           </p>
                           <p>- Luz Muñoz de Abril, Perú</p>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
         {/* End cards */}
      </>
   );
};

export { Welcome };

import React from "react";
import { Link } from "react-router-dom";

const NotFound = () => {
   return (
      <>
         <main>
            <section className="absolute w-full h-full">
               <div
                  className="absolute top-0 w-full h-full bg-gray-900"
                  style={{
                     backgroundImage:
                        "url(" +
                        require("../assets/images/register_bg_2.png").default +
                        ")",
                     backgroundSize: "100%",
                     backgroundRepeat: "no-repeat",
                  }}
               ></div>
               <div className="container mx-auto px-4 h-full">
                  <div className="flex content-center items-center justify-center h-full">
                     <div className="w-full lg:w-4/12 px-4">
                        <div className="relative flex flex-col min-w-0 break-words w-full mb-6 shadow-lg rounded-lg bg-gray-300 border-0">
                           <div className="flex-auto px-4 lg:px-10 py-10 pt-0">
                              <h6 className="text-2xl font-semibold">
                                 Esta página no existe!
                              </h6>
                              <Link to="/">
                                 <button
                                    class="bg-red-400 active:bg-pink-600 uppercase text-white font-bold hover:shadow-md shadow text-xs px-4 py-2 rounded outline-none focus:outline-none sm:mr-2 mb-1"
                                    type="button"
                                    style={{ transition: "all 0.15s ease 0s;" }}
                                 >
                                    Regresar al inicio
                                 </button>
                              </Link>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
         </main>
      </>
   );
};

export { NotFound };

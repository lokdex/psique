import React from "react";

import ContactoImg from "../assets/images/contacto.jpg";

const Contacto = () => {
   return (
      <>
         {/* Hero Begin */}
         <div
            className="relative pt-32 pb-32 flex content-center items-center justify-center"
            style={{
               minHeight: "20vh",
            }}
         >
            <div
               className="absolute top-0 w-full h-full bg-center bg-cover"
               style={{
                  backgroundImage: `url(${ContactoImg})`,
               }}
            >
               <span
                  id="blackOverlay"
                  className="w-full h-full absolute opacity-75 bg-black"
               ></span>
            </div>
            <div className="container relative mx-auto">
               <div className="items-center flex flex-wrap">
                  <div className="w-full lg:w-6/12 px-4 ml-auto mr-auto text-center">
                     <div className="pr-12">
                        <h1 className="text-white font-semibold text-4xl">
                           Contáctanos
                        </h1>
                     </div>
                  </div>
               </div>
            </div>
            <div
               className="top-auto bottom-0 left-0 right-0 w-full absolute pointer-events-none overflow-hidden"
               style={{ height: "70px", transform: "translateZ(0)" }}
            >
               <svg
                  className="absolute bottom-0 overflow-hidden"
                  xmlns="http://www.w3.org/2000/svg"
                  preserveAspectRatio="none"
                  version="1.1"
                  viewBox="0 0 2560 100"
                  x="0"
                  y="0"
               >
                  <polygon
                     className="text-gray-300 fill-current"
                     points="2560 0 2560 100 0 100"
                  ></polygon>
               </svg>
            </div>
         </div>
         {/* Hero End */}

         {/* Cards and small presentation */}
         <section className="pb-32 bg-gray-300 -mt-24">
            <div className="container mx-auto px-4 mt-24">
               <div className="flex flex-wrap">
                  <div className="w-full px-4 text-center mt-24">
                     <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-8 shadow-lg rounded-lg">
                        <div className="px-4 py-5 flex-auto">
                           <div className="text-center">
                              <div className="text-white p-3 text-center inline-flex items-center justify-center w-12 h-12 mb-5 shadow-lg rounded-full bg-red-400">
                                 <i className="fas fa-address-book"></i>
                              </div>
                              <div className="mb-5 text-gray-700">
                                 <p>Urb. Jorge Basadre A-3A, primer piso</p>
                                 <p>Cayma, Arequipa - Perú</p>
                              </div>
                              <div className="text-white p-3 text-center inline-flex items-center justify-center w-12 h-12 mb-5 shadow-lg rounded-full bg-red-400">
                                 <i className="fas fa-clock"></i>
                              </div>
                              <div className="mb-5 text-gray-700">
                                 <p>
                                    Lunes a Sábado de 9:00 am a 8:00 pm (GMT -5)
                                 </p>
                                 <p>Previa cita</p>
                              </div>
                              <div className="text-white p-3 text-center inline-flex items-center justify-center w-12 h-12 mb-5 shadow-lg rounded-full bg-red-400">
                                 <i className="fas fa-phone"></i>
                              </div>
                              <div className="mb-5 text-gray-700">
                                 <p>+51 959 940 249</p>
                              </div>
                              <div className="text-white p-3 text-center inline-flex items-center justify-center w-12 h-12 mb-5 shadow-lg rounded-full bg-red-400">
                                 <i className="fas fa-envelope"></i>
                              </div>
                              <div className="mb-2 text-gray-700">
                                 <p>info@psiqueymovimiento.com</p>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
         {/* End cards */}
      </>
   );
};

export { Contacto };

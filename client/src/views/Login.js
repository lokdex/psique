import React from "react";
import axios from "axios";
import { useHistory } from "react-router-dom";

const Login = () => {
   let history = useHistory();

   const submitLogin = async (e) => {
      e.preventDefault();
      let username = e.target.username.value;
      let password = e.target.password.value;

      try {
         const { data } = await axios.post(
            //"http://localhost:8080/webService.php",
            "http://psiqueymovimiento.com/webService.php",
            {
               QUERY: "IniciarSesion",
               USERNAME: username,
               PASSWORD: password,
            }
         );
         if (data.data) {
            sessionStorage.setItem("user", JSON.stringify(data.data));
            history.push("/crud-eventos");
         } else if (data.error) {
            alert(data.error);
         } else {
            throw new Error();
         }
      } catch {
         alert("Error al iniciar sesión");
      }
   };

   return (
      <>
         <main>
            <section className="absolute w-full h-full">
               <div
                  className="absolute top-0 w-full h-full bg-gray-900"
                  style={{
                     backgroundImage:
                        "url(" +
                        require("../assets/images/register_bg_2.png").default +
                        ")",
                     backgroundSize: "100%",
                     backgroundRepeat: "no-repeat",
                  }}
               ></div>
               <div className="container mx-auto px-4 h-full">
                  <div className="flex content-center items-center justify-center h-full">
                     <div className="w-full lg:w-4/12 px-4">
                        <div className="relative flex flex-col min-w-0 break-words w-full mb-6 shadow-lg rounded-lg bg-gray-300 border-0">
                           <div className="flex-auto px-4 lg:px-10 py-10 pt-0">
                              <form className="mt-5" onSubmit={submitLogin}>
                                 <div className="relative w-full mb-3">
                                    <label
                                       className="block uppercase text-gray-700 text-xs font-bold mb-2"
                                       htmlFor="grid-password"
                                    >
                                       Usuario
                                    </label>
                                    <input
                                       type="text"
                                       className="border-0 px-3 py-3 placeholder-gray-400 text-gray-700 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full"
                                       placeholder="Usuario"
                                       name="username"
                                       style={{ transition: "all .15s ease" }}
                                    />
                                 </div>

                                 <div className="relative w-full mb-3">
                                    <label
                                       className="block uppercase text-gray-700 text-xs font-bold mb-2"
                                       htmlFor="grid-password"
                                    >
                                       Contraseña
                                    </label>
                                    <input
                                       type="password"
                                       className="border-0 px-3 py-3 placeholder-gray-400 text-gray-700 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full"
                                       placeholder="Contraseña"
                                       name="password"
                                       style={{ transition: "all .15s ease" }}
                                    />
                                 </div>

                                 <div className="text-center mt-6">
                                    <button
                                       className="bg-gray-900 text-white active:bg-gray-700 text-sm font-bold uppercase px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 w-full"
                                       type="submit"
                                       style={{ transition: "all .15s ease" }}
                                    >
                                       Sign In
                                    </button>
                                 </div>
                              </form>
                           </div>
                        </div>
                        <div className="flex flex-wrap mt-6">
                           <div className="w-1/2">
                              <a
                                 href="#pablo"
                                 onClick={(e) => e.preventDefault()}
                                 className="text-gray-300"
                              >
                                 <small>Forgot password?</small>
                              </a>
                           </div>
                           <div className="w-1/2 text-right">
                              <a
                                 href="#pablo"
                                 onClick={(e) => e.preventDefault()}
                                 className="text-gray-300"
                              >
                                 <small>Create new account</small>
                              </a>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
         </main>
      </>
   );
};

export { Login };

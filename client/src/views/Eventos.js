import React, { useState, useEffect, Fragment } from "react";
import axios from "axios";

import ModalEvento from "../components/ModalEvento";

import EventosImg from "../assets/images/eventos.jpg";

const Eventos = () => {
   const [eventos, setEventos] = useState([]);
   const [evento, setEvento] = useState(null);
   const [show, setShow] = useState(false);

   const getEventos = async () => {
      try {
         const { data: response } = await axios.post(
            "http://psiqueymovimiento.com/webService.php",
            {
               QUERY: "getEventos",
            }
         );
         if (response.data) {
            setEventos(response.data);
         } else if (response.error) {
            alert(response.error);
            setEventos([]);
         } else {
            throw new Error();
         }
      } catch {
         alert(
            "Error al recuperar los eventos recientes, por favor inténtelo nuevamente en otro momento."
         );
      }
   };

   const handleOpenModal = (idx) => {
      setEvento(eventos[idx]);
      setShow(true);
   };

   useEffect(() => {
      getEventos();
   }, []);

   return (
      <>
         {/* Hero Begin */}
         <div
            className="relative pt-32 pb-32 flex content-center items-center justify-center"
            style={{
               minHeight: "20vh",
            }}
         >
            <div
               className="absolute top-0 w-full h-full bg-center bg-cover"
               style={{
                  backgroundImage: `url(${EventosImg})`,
               }}
            >
               <span
                  id="blackOverlay"
                  className="w-full h-full absolute opacity-75 bg-black"
               ></span>
            </div>
            <div className="container relative mx-auto">
               <div className="items-center flex flex-wrap">
                  <div className="w-full lg:w-6/12 px-4 ml-auto mr-auto text-center">
                     <div className="pr-12">
                        <h1 className="text-white font-semibold text-4xl">
                           Participa en nuestros eventos
                        </h1>
                     </div>
                  </div>
               </div>
            </div>
            <div
               className="top-auto bottom-0 left-0 right-0 w-full absolute pointer-events-none overflow-hidden"
               style={{ height: "70px", transform: "translateZ(0)" }}
            >
               <svg
                  className="absolute bottom-0 overflow-hidden"
                  xmlns="http://www.w3.org/2000/svg"
                  preserveAspectRatio="none"
                  version="1.1"
                  viewBox="0 0 2560 100"
                  x="0"
                  y="0"
               >
                  <polygon
                     className="text-gray-300 fill-current"
                     points="2560 0 2560 100 0 100"
                  ></polygon>
               </svg>
            </div>
         </div>
         {/* Hero End */}

         {/* Cards and small presentation */}
         <section className="pb-32 bg-gray-300 -mt-24">
            <div className="container mx-auto px-4 mt-24">
               <h3 className="text-3xl m-4 font-semibold leading-normal">
                  Eventos gratuitos
               </h3>
               <div className="flex flex-wrap">
                  <div className="w-full md:w-1/2 px-4 text-center mt-5">
                     <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-8 shadow-lg rounded-lg">
                        <div className="px-4 py-5 flex-auto">
                           <div className="text-white p-3 text-center inline-flex items-center justify-center w-12 h-12 mb-5 shadow-lg rounded-full bg-blue-400">
                              <i className="fas fa-calendar-check"></i>
                           </div>
                           <h6 className="text-2xl font-semibold">
                              La Maratón de la Felicidad
                           </h6>
                           <p className="mt-2 mb-4 text-gray-700">
                              Es una maravillosa oportunidad de talleres
                              diseñados para el fortalecimiento del Yo en las
                              áreas del desarrollo personal, emocional, físico,
                              familiar, social, laboral y espiritual. Son
                              talleres gratuitos, realizados por profesionales o
                              terapeutas nacionales e internacionales con
                              experiencia en procesos de acompañamiento
                              personal. El objetivo es contribuir con la salud
                              integral de la sociedad.
                           </p>
                        </div>
                     </div>
                  </div>

                  <div className="self-center w-full md:w-1/2 px-4 text-center">
                     <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-8 shadow-lg rounded-lg">
                        <div className="px-4 py-5 flex-auto">
                           <div className="text-white p-3 text-center inline-flex items-center justify-center w-12 h-12 mb-5 shadow-lg rounded-full bg-blue-400">
                              <i className="fas fa-calendar-check"></i>
                           </div>
                           <h6 className="text-2xl font-semibold">
                              Conferencias y charlas
                           </h6>
                           <p className="mt-2 mb-4 text-gray-700">
                              Las conferencias y charlas son realizadas online,
                              y abordan temas de la psicología y la vida humana
                              para ayudar a los asistentes a conocer mejore su
                              propia realidad personal para su mejora continua
                           </p>
                           <p className="mt-2 mb-4 text-gray-700">
                              Son eventos gratuitos previa inscripción.
                           </p>
                        </div>
                     </div>
                  </div>
               </div>
               {!!eventos.length && (
                  <h3 className="text-3xl m-4 font-semibold leading-normal">
                     Próximos eventos
                  </h3>
               )}
               <div className="flex flex-wrap">
                  {!!eventos.length ? (
                     eventos.map((item, idx) => (
                        <div className="w-full md:w-1/2 px-4 text-center mt-5">
                           <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-8 shadow-lg rounded-lg">
                              <div className="px-4 py-5 flex-auto">
                                 <div className="text-white p-3 text-center inline-flex items-center justify-center w-12 h-12 mb-5 shadow-lg rounded-full bg-red-400">
                                    <i className="fas fa-calendar-check"></i>
                                 </div>
                                 <h6 className="text-2xl font-semibold">
                                    {item?.titulo}
                                 </h6>
                                 <p
                                    className="mt-2 mb-4 text-gray-700"
                                    style={{ whiteSpace: "pre-line" }}
                                 >
                                    {item?.resumen}
                                 </p>
                                 <div class="w-full w-4/12 px-4 self-center">
                                    <div class="px-3 mt-32 sm:mt-0">
                                       <button
                                          class="bg-red-400 active:bg-pink-600 uppercase text-white font-bold hover:shadow-md shadow text-xs px-4 py-2 rounded outline-none focus:outline-none sm:mr-2 mb-1"
                                          type="button"
                                          style={{
                                             transition: "all 0.15s ease 0s;",
                                          }}
                                          onClick={() => handleOpenModal(idx)}
                                       >
                                          Ver más detalles
                                       </button>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     ))
                  ) : (
                     <></>
                  )}
               </div>
            </div>
         </section>
         {/* End cards */}
         <ModalEvento show={show} setShow={setShow} evento={evento} />
      </>
   );
};

export { Eventos };
